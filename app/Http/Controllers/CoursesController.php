<?php

namespace App\Http\Controllers;

use App\Models\Courses;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Courses::with('department')->paginate();
        return view('dashboard.courses.index',compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();

        return view('dashboard.courses.create',compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'name' => 'required|string',
            'info' => 'required|string',
            'location' => 'required|string',
            'dep_id' => 'required|exists:departments,id',
            'start_date' => 'nullable|date',
            'price' => 'nullable|string',
            'cover' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
            'icon' => 'nullable|image|mimes:png,gif,svg|max:2048',

        ]);
        $path = public_path('courses_cover/');
        $filename = time(). 'course'.'.webp';
        $coverFile = $request->file('cover');


        $coverFile->move($path, $filename);


        $cover = $filename;
        Courses::create([
            'name' => $request->get('name'),
            'info' => $request->get('info'),
            'location' => $request->get('location'),
            'dep_id' => $request->get('dep_id'),
            'price' => $request->get('price'),
            'start_date' => $request->get('start_date'),
            'cover' => $cover,
            'icon' => '',
            'status' => 0,
        ]);

        return redirect()->route('courses.index')->with('success','Course added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function show(Courses $course)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function edit(Courses $course)
    {
        $course->load('department');
        $departments = Department::all();
        return view('dashboard.courses.edit',compact('departments','course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Courses $course)
    {
        $this->validate($request,[
            'name' => 'required|string',
            'info' => 'required|string',
            'location' => 'required|string',
            'dep_id' => 'required|exists:departments,id',
            'start_date' => 'nullable|date',
            'price' => 'nullable|string',
            'cover' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
            'icon' => 'nullable|image|mimes:png,gif,svg|max:2048',
        ]);
        $cover = $course->cover;
        if ( $request->has('cover')){
            if (Storage::disk('public')->exists($course->cover)){
                Storage::disk('public')->delete($course->cover);
            }
            $coverFile = $request->file('cover');
            $filename = time().$coverFile->getClientOriginalName();
            $path = public_path('courses_cover/');
            $coverFile->move($path, $filename);

            $cover = $filename;
        }
        $icon = $course->icon;
        if ( $request->has('icon')){
            if (Storage::disk('public')->exists($course->icon)){
                Storage::disk('public')->delete($course->icon);
            }
            $iconFile = $request->file('icon');
            $iconFilename = time().$iconFile->getClientOriginalName();
            $path2 = public_path('courses_icons/');
            $iconFile->move($path2, $iconFilename);
            $icon =$iconFilename;
        }



       $course->fill([
           'name' => $request->get('name'),
           'info' => $request->get('info'),
           'location' => $request->get('location'),
           'dep_id' => $request->get('dep_id'),
           'price' => $request->get('price'),
           'start_date' => $request->get('start_date'),
           'cover' => $cover,
           'icon' => $icon,

       ])->save();

        return redirect()->route('courses.index')->with('success','Course Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function destroy(Courses $course)
    {
        $path = public_path($course->cover);
        if (File::exists($path)){
            File::delete($course->cover);
        }

        $path =  public_path($course->icon);
        if (File::exists($path)){
            File::delete($course->cover);
        }

        $course->delete();

        return redirect()->route('courses.index')->with('success','Course Deleted Successfully');
    }


}
