<?php

namespace App\Http\Controllers;

use App\Models\Instructor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class InstructorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $instructors = Instructor::paginate();
        return view('dashboard.instructor.index', compact('instructors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.instructor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'img' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
            'name' => 'required|string',
            'info' => 'required|string',
            'position' => 'required|string',
        ]);

        $image1File = $request->file('img');
        $filename1 = time() . $image1File->getClientOriginalName();
        $path = public_path('instructor_image/');
        $image1File->move($path, $filename1);

        Instructor::create([
            'name' => $request->get('name'),
            'info' => $request->get('info'),
            'position' => $request->get('position'),
            'img' => $filename1,
        ]);

        return redirect()->route('instructor.index')->with('success', 'Instructor added Successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Instructor  $instructor
     * @return \Illuminate\Http\Response
     */
    public function show(Instructor $instructor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Instructor  $instructor
     * @return \Illuminate\Http\Response
     */
    public function edit(Instructor $instructor)
    {
        return view('dashboard.instructor.edit',compact('instructor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Instructor  $instructor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Instructor $instructor)
    {
        $this->validate($request,[
            'img' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
            'name' => 'required|string',
            'info' => 'required|string',
            'position' => 'required|string',
        ]);

        $path = public_path('instructor_image/');

        $img = $instructor->img;

        if ($request->has('img')) {

            if (File::exists($path.$img)) {
                File::delete($path.$img);
            }

            $image1File = $request->file('img');
            $filename1 = time() . $image1File->getClientOriginalName();
            $image1File->move($path, $filename1);
            $img =  $filename1;

        }

        $instructor->fill([
            'img' => $img,
            'name' => $request->get('name'),
            'position' => $request->get('position'),
            'info' => $request->get('info'),
        ])->save();
        return redirect()->route('instructor.index')->with('success', 'Instructor Updated Successfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Instructor  $instructor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Instructor $instructor)
    {

        $path = public_path('instructor_image/');
        if (File::exists($path.$instructor->img)) {
            File::delete($path.$instructor->img);
        }
        $instructor->delete();

        return redirect()->route('instructor.index')->with('success', 'Instructor Deleted Successfully');

    }
}
