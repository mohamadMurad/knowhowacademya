<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newses = News::paginate();
        return  view('dashboard.news.index',compact('newses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('dashboard.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNewsRequest $request)
    {

        $news = News::create([
            'title' => $request->title,
            'slug' => Str::slug($request->title,'-'),
            'body' => $request->get('content'),
        ]);


        if($request->hasFile('cover')){
            $file = $this->store_file([
                'source'=>$request['cover'],
                'validation'=>"image",
                'path_to_save'=>'/uploads/news/',
                'type'=>'IMAGE',
                'user_id'=>Auth::user()->id,
                //'resize'=>[500,1000],
                'small_path'=>'small/',
                'visibility'=>'PUBLIC',
                'file_system_type'=>env('FILESYSTEM_DRIVER','public'),
                'optimize'=>false
            ])['filename'];
            $news->update([
                'cover' => $file,
            ]);
        }

        return redirect()->route('news.index')->with('success','News added Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {

        return  view('dashboard.news.edit',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNewsRequest $request, News $news)
    {
        $news->update([
            'title' => $request->title,
            'body' => $request->get('content'),
        ]);

        if($request->hasFile('cover')){
            if(Storage::disk('public')->exists("/public/uploads/news/" . $news->getAttributes()['cover'])){

                $file = Storage::disk('public')->delete("/public/uploads/news/" . $news->getAttributes()['cover']);

            }
            $file = $this->store_file([
                'source'=>$request['cover'],
                'validation'=>"image",
                'path_to_save'=>'/uploads/news/',
                'type'=>'IMAGE',
                'user_id'=>Auth::user()->id,
                //'resize'=>[500,1000],
                'small_path'=>'small/',
                'visibility'=>'PUBLIC',
                'file_system_type'=>env('FILESYSTEM_DRIVER','public'),
                'optimize'=>false
            ])['filename'];
            $news->update([
                'cover' => $file,
            ]);
        }


        return redirect()->route('news.index')->with('success','News Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        if(Storage::disk('public')->exists("/public/uploads/news/" . $news->getAttributes()['cover'])){

            $file = Storage::disk('public')->delete("/public/uploads/news/" . $news->getAttributes()['cover']);

        }
        $news->delete();
        return redirect()->route('news.index')->with('success','News Deleted Successfully');

    }
}
