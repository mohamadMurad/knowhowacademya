<?php

namespace App\Http\Controllers;

use App\Models\AccreditCenter;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use function GuzzleHttp\Promise\all;

class AccreditCenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = AccreditCenter::paginate();
        return view('dashboard.accreditCenter.index', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.accreditCenter.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'image1' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
        ]);
        $model = AccreditCenter::create([
            'name' => $request->get('name'),
            'address' => $request->get('address'),
            'phone' => $request->get('phone'),
        ]);
        $model->addMediaFromRequest('image1')->toMediaCollection('accreditCenter');
        return redirect()->route('accreditCenter.index')->with('success', 'Accredit Center added Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\Response
     */
    public function show(AccreditCenter $accreditCenter)
    {
        return view('front.department', compact('accreditCenter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\Response
     */
    public function edit(AccreditCenter $accreditCenter)
    {
        return view('dashboard.accreditCenter.edit', compact('accreditCenter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AccreditCenter $accreditCenter)
    {

        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'image1' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
        ]);


        if ($request->has('image1')) {
            $accreditCenter->clearMediaCollection('accreditCenter');
            $accreditCenter->addMediaFromRequest('image1')->toMediaCollection('accreditCenter');
        }

        $accreditCenter->fill([
            'name' => $request->get('name'),
            'address' => $request->get('address'),
            'phone' => $request->get('phone'),
        ])->save();
        return redirect()->route('accreditCenter.index')->with('success', 'Accredit Center Updated Successfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(AccreditCenter $accreditCenter)
    {
        $accreditCenter->getFirstMedia('accreditCenter')?->delete();
        $accreditCenter->delete();

        return redirect()->route('accreditCenter.index')->with('success', 'Accredit Center Deleted Successfully');

    }
}
