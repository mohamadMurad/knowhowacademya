<?php

namespace App\Http\Controllers;

use App\Models\Certificate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $certificate = Certificate::paginate();
        return view('dashboard.certificate.index', compact('certificate'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.certificate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'cert_id' => 'required|unique:certificates',
            'info' => 'nullable|string',
            'file' => 'required|file|mimetypes:application/pdf|max:1024',
        ]);



        $certificate = Certificate::create([
            'cert_id' => $request->get('cert_id'),
            'info' => $request->get('info'),
            'uuid' => Str::uuid()->toString(),
            'file' => '',
        ]);

        if ($request->hasFile('file')) {
            $file = $this->store_file([
                'source' => $request['file'],
                'validation' => "image",
                'path_to_save' => '/uploads/certificates/',
                'type' => 'PDF',
                'user_id' => Auth::user()->id,
                //'resize'=>[500,1000],
                'small_path' => 'small/',
                'visibility' => 'PUBLIC',
                'file_system_type' => env('FILESYSTEM_DRIVER', 'public'),
                'optimize' => false
            ])['filename'];
            $certificate->update([
                'file' => $file,
            ]);
        }
        return redirect()->route('certificates.index')->with('success', 'Certificate added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Certificate $certificate
     * @return \Illuminate\Http\Response
     */
    public function show(Certificate $certificate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Certificate $certificate
     * @return \Illuminate\Http\Response
     */
    public function edit(Certificate $certificate)
    {
        return view('dashboard.certificate.edit', compact('certificate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Certificate $certificate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Certificate $certificate)
    {
        $this->validate($request, [
            'cert_id' => 'required|unique:certificates,cert_id,' . $certificate->id,
            'info' => 'nullable|string',
            'file' => 'nullable|file|mimetypes:application/pdf|max:1024',
        ]);

        $certificate->fill([
            'cert_id' => $request->get('cert_id'),
            'info' => $request->get('info'),
        ])->save();


        if ($request->hasFile('file')) {
            if(Storage::disk('public')->exists("/public/uploads/certificates/" .  $certificate->getAttributes()['file'])){

                $file = Storage::disk('public')->delete("/public/uploads/certificates/" .  $certificate->getAttributes()['file']);

            }

            $file = $this->store_file([
                'source' => $request['file'],
                'validation' => "image",
                'path_to_save' => '/uploads/certificates/',
                'type' => 'PDF',
                'user_id' => Auth::user()->id,
                //'resize'=>[500,1000],
                'small_path' => 'small/',
                'visibility' => 'PUBLIC',
                'file_system_type' => env('FILESYSTEM_DRIVER', 'public'),
                'optimize' => false
            ])['filename'];
            $certificate->update([
                'file' => $file,
            ]);
        }

        return redirect()->route('certificates.index')->with('success', 'Certificate Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Certificate $certificate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Certificate $certificate)
    {

        if(Storage::disk('public')->exists("/public/uploads/certificates/" . $certificate->getAttributes()['file'])){

            $file = Storage::disk('public')->delete("/public/uploads/certificates/" . $certificate->getAttributes()['file']);

        }
        $certificate->delete();

        return redirect()->route('certificates.index')->with('success', 'Certificate Deleted Successfully');


    }

    public function check_certificate(Request $request)
    {

        $this->validate($request, [
            'cert_id' => 'required',
        ]);
        $cert_id = $request->get('cert_id');
        $cert = Certificate::all()->where('cert_id', $cert_id)->first();
        //print_r($cert);
        if ($cert) {
            return  redirect()->route('front.certificate.verify',$cert->uuid);
            return response()->json([
                'data' => [
                    'status' => true,
                    'info' => $cert->info,
                    'msg' => 'This certificate is valid'
                ]
            ], 200);
        }
        return  redirect()->route('front.certificate.verify',null);
        return response()->json([
            'data' => [
                'status' => false,
                'msg' => 'This certificate is not valid'
            ]
        ], 200);

    }
}
