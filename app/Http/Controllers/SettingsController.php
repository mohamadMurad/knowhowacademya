<?php

namespace App\Http\Controllers;

use App\Models\settings;
use Buglinjo\LaravelWebp\Facades\Webp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class SettingsController extends Controller
{

    public function index(){
        $siteDescription = settings::get('site_desc');
        $siteKeywords= settings::get('site_keywords');
        $whatsappNumber = settings::get('whatsapp_number');
        $contactEmail = settings::get('contact_email');
        $contactPhone = settings::get('contact_phone');
        $robots_txt = settings::get('robots_txt');
        $siteInsta = settings::get('instagram');
        $siteFacebook = settings::get('facebook');
        $Linkedin = settings::get('Linkedin');

        $siteInstaName = settings::get('instagramName');
        $siteFacebookName = settings::get('facebookName');
        $LinkedinName = settings::get('LinkedinName');

        $address1 = settings::get('address1');
        $address2 = settings::get('address2');
        $header_img = settings::get('header_img');
        $contactUsDesc = settings::get('contactUsDesc');
        $contactUsEmail = settings::get('contactUsEmail');
        $aboutUs = settings::get('aboutUs');


        return view('dashboard.settings.index',compact('siteDescription',
            'siteKeywords','whatsappNumber','contactEmail','Linkedin','LinkedinName',
            'contactPhone','robots_txt',
            'siteFacebook','siteInsta','siteInstaName','siteFacebookName',
        'address1','address2','header_img','contactUsDesc','contactUsEmail','aboutUs'));
    }

    public function update (Request $request){

        $this->validate($request,[
            'whatsapp_number'=>'required',
            'contact_email'=>'required|email',
            'contact_phone'=>'required',
            'site_keywords'=>'required|string',
            'site_desc'=>'required|string',
           // 'robots_txt' => 'required|string',
            'instagram' => 'required|string',
            'instagramName' => 'required|string',
            'facebook' => 'required|string',
            'facebookName' => 'required|string',
            'Linkedin' => 'nullable|string',
            'LinkedinName' => 'nullable|string',
            'address1' => 'required|string',
            'contactUsEmail' => 'required|email',
            'contactUsDesc' => 'required|string',
            'address2' => 'nullable|string',
            'aboutUs' => 'required|string',
            'header_img' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
        ]);

        $path = public_path('header_image/');
        $header_img = settings::get('header_img');
        $header_og_img = settings::get('header_og_img');

        if ($request->hasFile('header_img')){
            if (File::exists($path . $header_img)) {
                File::delete($path . $header_img);
            }
            if (File::exists($path . $header_og_img)) {
                File::delete($path . $header_og_img);
            }
            $image1File = $request->file('header_img');

            $header_img = time() . 'header'.'.webp';
            $header_og_img = time() . Str::slug('ogImage') . '.' .$image1File->clientExtension();

            // desktop
            $img = Image::make($image1File->path());
            $img->encode('webp', 90)->resize(530, 664, function ($const) {
                $const->aspectRatio();
            })->save($path.'/'.$header_img );

            // og
            $img = Image::make($image1File->path());
            $img->resize(1200, 630, function ($const) {
             //   $const->aspectRatio();
            })->save($path.'/'.$header_og_img);


          //  $image1File->move($path, $header_img);
        }





        settings::set('whatsapp_number',$request->get('whatsapp_number'));
        settings::set('contact_email',$request->get('contact_email'));
        settings::set('contact_phone',$request->get('contact_phone'));
        settings::set('site_keywords',$request->get('site_keywords'));
        settings::set('site_desc',$request->get('site_desc'));
       // settings::set('robots_txt',$request->get('robots_txt'));
        settings::set('facebook',$request->get('facebook'));
        settings::set('facebookName',$request->get('facebookName'));
        settings::set('instagram',$request->get('instagram'));
        settings::set('instagramName',$request->get('instagramName'));

        settings::set('Linkedin',$request->get('Linkedin'));
        settings::set('LinkedinName',$request->get('LinkedinName'));

        settings::set('address1',$request->get('address1'));
        settings::set('address2',$request->get('address2'));
        settings::set('header_img', $header_img);
        settings::set('header_og_img', $header_og_img);
        settings::set('contactUsDesc', $request->get('contactUsDesc'));
        settings::set('contactUsEmail', $request->get('contactUsEmail'));
        settings::set('aboutUs', $request->get('aboutUs'));


        return redirect()->route('settings.index')->with('success','Settings Updated Successfully');
    }
}
