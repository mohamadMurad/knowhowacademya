<?php

namespace App\Http\Controllers;

use App\Mail\ContactUs;
use App\Mail\ContactUsAdmin;
use App\Models\AccreditCenter;
use App\Models\Branches;
use App\Models\Certificate;
use App\Models\Chat;
use App\Models\Courses;
use App\Models\Department;
use App\Models\Instructor;
use App\Models\News;
use App\Models\settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;

class FrontController extends Controller
{

    public function __construct()
    {

        $siteDescription = settings::get('site_desc');
        $siteKeywords = settings::get('site_keywords');
        $whatsappNumber = settings::get('whatsapp_number');
        $contactEmail = settings::get('contact_email');
        $contactPhone = settings::get('contact_phone');
        $siteType = settings::get('site_type');

        $siteInsta = settings::get('instagram');
        $siteFacebook = settings::get('facebook');

        $siteLinkedName = settings::get('LinkedinName');
        $siteLinked = settings::get('Linkedin');

        $siteInstaName = settings::get('instagramName');
        $siteFacebookName = settings::get('facebookName');

        $address1 = settings::get('address1');
        $address2 = settings::get('address2');
        $contactUsDesc = settings::get('contactUsDesc');

        $departments = \App\Models\Department::all();

        $header_og_img = settings::get('header_og_img');
        $chats = Chat::all();
        $header_img = settings::get('header_img');

        $centers = AccreditCenter::latest()->limit(4)->get();

        View::share(compact('siteDescription', 'siteLinked', 'siteLinkedName',
            'siteKeywords', 'whatsappNumber', 'contactEmail',
            'contactPhone', 'siteType', 'departments', 'siteInsta', 'siteFacebook',
            'siteFacebookName', 'siteInstaName', 'address1', 'address2', 'chats','centers', 'header_img', 'header_og_img', 'contactUsDesc'));
    }

    public function branches()
    {
        $branches = Branches::all()->groupBy('city');

        return view('front.branches', compact('branches'));
    }

    public function centers()
    {
        $centers = AccreditCenter::all();

        return view('front.centers', compact('centers'));
    }

    public function showCourse($course)
    {
        $course = Courses::where('slug', $course)->firstOrFail();
        $course->load('department');
        return view('front.course', compact('course'));
    }

    public function contactUs()
    {
        return view('front.contact');
    }

    public function aboutUs()
    {
        $aboutUs = settings::get('aboutUs');
        return view('front.about', compact('aboutUs'));
    }

    public function index()
    {

        $newses = News::latest()->limit(3)->get();
        $courses = \App\Models\Courses::with('department')->latest()->limit(5)->get();
        $upcomingCourses = \App\Models\Courses::upcoming()->get();
        $locations = \App\Models\Courses::select(DB::raw('DISTINCT location'))->get();
        return view('front.home', compact('courses', 'upcomingCourses', 'newses', 'locations'));

    }


    public function showDep($department)
    {

        $department = Department::where('slug', $department)->with('courses_all')->firstOrFail();
        $courses = $department->courses_all()->paginate();

        return view('front.department', compact('department', 'courses'));
    }

    public function search(Request $request)
    {


        $courses = Courses::query();
        if ($request->has('department') && $request->get('department') != 'null') {
            $courses->where('dep_id', $request->get('department'));
        }
        if ($request->has('location') && $request->get('location') != 'null') {
            $courses->where('location', $request->get('location'));
        }
        if ($request->has('month') && $request->get('month') != 'null') {
            $courses->whereMonth('start_date', $request->get('month'));

        }
        if ($request->has('all') && $request->get('all') != 'null') {

            //$courses->where('location',$request->get('location'));
        }

        if ($request->has('name') && $request->get('name') != 'null') {
            $courses->where('name', 'like', "%" . $request->get('name') . "%");
        }


//        $courses->getRelation('department');
        // dd($courses->with('department')->get());
        $result = $courses->with('department')->get();
        return view('front.search', compact('result'));
    }

    function instructors(Request $request)
    {
        // $instructors = Instructor::where('status', 'CERTIFIED')->get();
        $response = Http::get('https://portal.knowhowacademya.uk/api/getTrainers');
//        $response = Http::get('http://127.0.0.1:8001/api/getTrainers/');
        if ($response->status() == 404) {
            return redirect()->route('home');
        }
        $instructors = $response->object();

        return view('front.instructors.instructors', compact('instructors'));
    }

    function instructorsShow(Request $request, Instructor $instructor)
    {
//        if ($instructor->status != 'CERTIFIED') {
//            return redirect()->route('instructors');
//        }

        $response = Http::get('https://portal.knowhowacademya.uk/api/getTrainer/' . $instructor->id);
//        $response = Http::get('http://127.0.0.1:8001/api/getTrainer/' . $instructor->id);

        if ($response->status() == 404) {
            return redirect()->route('instructors');
        }
        $instructor = $response->object();
        return view('front.instructors.show', compact('instructor'));
    }

    public function submitContactUs(Request $request)
    {


        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email',
            'subject' => 'required|string',
            'message' => 'required|string',
        ]);


//        Mail::to($request->get('email'))
//            ->send(new ContactUs(['name' => $request->get('name')]));


        $to = 'Knowhowacademya@gmail.com';
        if ($contactUsEmail = settings::get('contactUsEmail')) {
            $to = $contactUsEmail;
        }

        Mail::to($to)
            ->send(new ContactUsAdmin([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'subject' => $request->get('subject'),
                'msg' => $request->get('message'),
            ]));


        return redirect()->back()->with('success', 'Your message has been sent. Thank you!');
    }

    public function news()
    {
        $newses = News::paginate();
        return view('front.news.index', compact('newses'));
    }

    public function newsShow($news)
    {
        $news = News::where('id', $news)->firstOrFail();
        return view('front.news.show', compact('news'));
    }


    public function certificate_verify($uuid = null)
    {
        $certificate = Certificate::where('uuid', $uuid)->first();

        return view('front.certificate.info', compact('certificate'));
    }

    public function certificate_show($uuid = null)
    {
        $certificate = Certificate::where('uuid', $uuid)->first();
        if (!$certificate || !$certificate->file) {
            return redirect()->route('home');
        }

        return view('front.certificate.show', compact('certificate'));
    }
}
