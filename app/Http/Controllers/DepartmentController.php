<?php

namespace App\Http\Controllers;

use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use function GuzzleHttp\Promise\all;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::paginate();
        return view('dashboard.departments.index', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request, [
            'name' => 'required|unique:departments',
            'desc' => 'nullable|string',
//            'courses' => 'required',
//            'courses.names.*' => 'required|string',
//            'courses.desc.*' => 'required|string',
            'image1' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
            'image2' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
            'image3' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
            'image4' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
        ]);

        $image1File = $request->file('image1');
//        $image2File = $request->file('image2');
//        $image3File = $request->file('image3');
//        $image4File = $request->file('image4');


        $filename1 = time() . $image1File->getClientOriginalName();
//        $filename2 = time() . $image2File->getClientOriginalName();
//        $filename3 = time() . $image3File->getClientOriginalName();
//        $filename4 = time() . $image4File->getClientOriginalName();

        $path = public_path('department_image/');
        $image1File->move($path, $filename1);
//        $image2File->move($path, $filename2);
//        $image3File->move($path, $filename3);
//        $image4File->move($path, $filename4);


        Department::create([
            'name' => $request->get('name'),
            'desc' => $request->get('desc'),
            'image1' => $filename1,
            'image2' => '$filename2',
            'image3' => '$filename3',
            'image4' => '$filename4',
            'courses' => ''//json_encode($request->get('courses'))
        ]);

        return redirect()->route('departments.index')->with('success', 'Departments added Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\Response
     */
    public function show($department)
    {
        $department = Department::where('slug',$department)->firstOrFail();

        return view('front.department', compact('department'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        return view('dashboard.departments.edit', compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {

        $this->validate($request, [
            'name' => 'required|unique:departments,id,' . $department->id,
            'desc' => 'nullable|string',
//            'courses' => 'required',
//            'courses.names.*' => 'required|string',
//            'courses.desc.*' => 'required|string',
            'image1' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
            'image2' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
            'image3' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
            'image4' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
        ]);

        $path = public_path('department_image/');
        $image1 = $department->image1;
        if ($request->has('image1')) {
            if (File::exists($path . $image1)) {
                File::delete($path . $image1);
            }

            $image1File = $request->file('image1');
            $filename1 = time() . $image1File->getClientOriginalName();
            $image1File->move($path, $filename1);
            $image1 = $filename1;

        }
        $image2 = $department->image2;
        if ($request->has('image2')) {

            if (File::exists($path . $image2)) {
                File::delete($path . $image2);
            }

            $image2File = $request->file('image2');
            $filename2 = time() . $image2File->getClientOriginalName();
            $image2File->move($path, $filename2);

            $image2 = $filename2;
        }
        $image3 = $department->image3;
        if ($request->has('image3')) {
            if (File::exists($path . $image3)) {
                File::delete($path . $image3);
            }


            $image3File = $request->file('image3');
            $filename3 = time() . $image3File->getClientOriginalName();
            $image3File->move($path, $filename3);
            $image3 = $filename3;

        }
        $image4 = $department->image4;
        if ($request->has('image4')) {

            if (File::exists($path . $image4)) {
                File::delete($path . $image4);
            }


            $image4File = $request->file('image4');
            $filename4 = time() . $image4File->getClientOriginalName();
            $image4File->move($path, $filename4);
            $image4 = $filename4;
        }

        $department->fill([
            'name' => $request->get('name'),
            'desc' => $request->get('desc'),
            'courses' => '',//json_encode($request->get('courses')),
            'image1' => $image1,
            'image2' => $image2,
            'image3' => $image3,
            'image4' => $image4,

        ])->save();
        return redirect()->route('departments.index')->with('success', 'Departments Updated Successfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        $path = public_path('department_image/');
        if (File::exists($path . $department->image1)) {
            File::delete($path . $department->image1);
        }

        if (File::exists($path . $department->image2)) {
            File::delete($path . $department->image2);
        }
        if (File::exists($path . $department->image3)) {
            File::delete($path . $department->image3);
        }
        if (File::exists($path . $department->image4)) {
            File::delete($path . $department->image4);
        }

        $department->delete();

        return redirect()->route('departments.index')->with('success', 'Departments Deleted Successfully');

    }
}
