<?php

namespace App\Http\Controllers;

use App\Models\settings;
use Illuminate\Http\Request;

class HelperController extends Controller
{
    public function robots(){
        $robots_txt = settings::get('robots_txt');
        return response($robots_txt)->header('Content-Type', 'text/plain');
    }
}
