<?php

namespace App\Http\Controllers;

use App\Models\Branches;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BranchesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = Branches::paginate();

        return view('dashboard.branches.index',compact('branches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.branches.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=> 'required|string',
            'city' => 'required|string',
            'phone' => 'nullable|string',
            'agent' => 'nullable|string',
            'address' => 'nullable|string',
        ]);

        Branches::create([
            'name' => $request->get('name'),
            'city' => Str::lower($request->get('city')),
            'phone' => $request->get('phone'),
            'agent' => $request->get('agent'),
            'address' => $request->get('address'),
        ]);

        return redirect()->route('branches.index')->with('success','Branch added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Branches  $branches
     * @return \Illuminate\Http\Response
     */
    public function show(Branches $branches)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Branches  $branches
     * @return \Illuminate\Http\Response
     */
    public function edit(Branches $branch)
    {
        return view('dashboard.branches.edit',compact('branch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Branches  $branches
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Branches $branch)
    {
        $this->validate($request,[
            'name'=> 'required|string',
            'city' => 'required|string',
            'phone' => 'nullable|string',
            'agent' => 'nullable|string',
            'address' => 'nullable|string',
        ]);

        $branch->fill([
            'name' => $request->get('name'),
            'city' => Str::lower($request->get('city')),
            'phone' => $request->get('phone'),
            'agent' => $request->get('agent'),
            'address' => $request->get('address'),
        ])->save();

        return redirect()->route('branches.index')->with('success','Branch Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Branches  $branches
     * @return \Illuminate\Http\Response
     */
    public function destroy(Branches $branch)
    {
        $branch->delete();
        return redirect()->route('branches.index')->with('success','Branch Deleted Successfully');
    }
}
