<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class settings extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'payload'
    ];

    public static function get($name){

        $setting = self::query()
            ->where('name', $name)
            ->first('payload');

        if ($setting){
            return $setting->getAttribute('payload');
        }

        return '';

    }

    public static function set($name,$payload){
        self::query()->updateOrCreate([
            'name' => $name,
        ],[
            'payload' => $payload,
        ]);
    }


}
