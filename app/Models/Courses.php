<?php

namespace App\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    use HasFactory;
    use Sluggable;

    protected $fillable = [
        'name',
        'price',
        'slug',
        'location',
        'dep_id',
        'status',
        'cover',
        'icon',
        'start_date',
        'info',
    ];

    public function department()
    {
        return $this->belongsTo(Department::class, 'dep_id', 'id');
    }

    public function scopeUpcoming($query)
    {

        return $query->where('start_date', '>', Carbon::today());
    }




    public function getStartDateAttribute(){
        if(!$this->attributes['start_date']){
           return null;
        }
        return \Carbon\Carbon::make($this->attributes['start_date'])->format('Y-m-d');
    }
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['name', 'id']
            ]
        ];
    }
}
