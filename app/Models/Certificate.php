<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Certificate extends Model
{
    use HasFactory;

    protected $fillable = [
        'cert_id',
        'info',
        'uuid',
        'file'
    ];


    public function getFileAttribute()
    {

        $url = env("STORAGE_URL") . "/uploads/certificates/" . $this->attributes['file'];
        if (Storage::disk('public')->exists("/public/uploads/certificates/" . $this->attributes['file'])) {

            $file = Storage::disk('public')->get("/public/uploads/certificates/" . $this->attributes['file']);

            $pdf = base64_encode($file);

            return $pdf;
        }


        return null;

    }

    public function getFilePathAttribute()
    {

        $url = env("STORAGE_URL") . "/uploads/certificates/" . $this->attributes['file'];
        return $url;
        if (Storage::disk('public')->exists("/public/uploads/certificates/" . $this->attributes['file'])) {

            $file = Storage::disk('public')->get("/public/uploads/certificates/" . $this->attributes['file']);

            $pdf = base64_encode($file);

            return $pdf;
        }


        return null;

    }
}
