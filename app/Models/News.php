<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class News extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'body', 'cover', 'slug'
    ];


// Carbon instance fields
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getCoverAttribute()
    {
        ;
        return $this->attributes['cover'] ?
            env("STORAGE_URL") . "/uploads/news/" . $this->attributes['cover']
            : asset('img/header-img.webp');
    }

    public function getShortTitleAttribute()
    {
        return \Illuminate\Support\Str::limit(trim(preg_replace('/\s\s+/', ' ', strip_tags($this->body))), 100, $end = '...');
    }

}
