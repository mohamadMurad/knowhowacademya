<?php

use App\Http\Controllers\BackendHelperController;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['auth'],'prefix'=>'admin'], function (){
    Route::get('/dashboard', function () {
        $coursesCount = \App\Models\Courses::count();
        $CertificateCount = \App\Models\Certificate::count();
        $InstructorCount = \App\Models\Instructor::count();
        return view('dashboard.dashboard.index',compact('coursesCount','CertificateCount','InstructorCount'));
    })->middleware(['auth'])->name('dashboard');

    Route::resource('news',\App\Http\Controllers\NewsController::class);
    Route::resource('certificates',\App\Http\Controllers\CertificateController::class);
    Route::resource('departments',\App\Http\Controllers\DepartmentController::class)->except('show');
    Route::resource('accreditCenter',\App\Http\Controllers\AccreditCenterController::class)->except('show');
    Route::resource('courses',\App\Http\Controllers\CoursesController::class);
    Route::get('setting',[\App\Http\Controllers\SettingsController::class,'index'])->name('settings.index');
    Route::post('setting',[\App\Http\Controllers\SettingsController::class,'update'])->name('settings.update');
    Route::post('setting',[\App\Http\Controllers\SettingsController::class,'update'])->name('settings.update');
//    Route::resource('instructor',\App\Http\Controllers\InstructorController::class);
    Route::resource('chats',\App\Http\Controllers\ChatController::class);
    Route::resource('branches',\App\Http\Controllers\BranchesController::class);

    Route::prefix('upload')->name('upload.')->group(function(){
        Route::post('/image',[BackendHelperController::class,'upload_image'])->name('image');
        Route::post('/file',[BackendHelperController::class,'upload_file'])->name('file');
        Route::post('/remove-file',[BackendHelperController::class,'remove_files'])->name('remove-file');
    });

});

Route::get('/', [\App\Http\Controllers\FrontController::class,'index'])->name('home');
Route::get('department/{department}',[\App\Http\Controllers\FrontController::class,'showDep'])->name('department.show');
Route::get('search',[\App\Http\Controllers\FrontController::class,'search'])->name("search");



Route::get('robots.txt',[\App\Http\Controllers\HelperController::class,'robots']);
//Route::get('manifest.json',[HelperController::class,'manifest']);
Route::get('sitemap.xml',[\App\Http\Controllers\SiteMapController::class,'sitemap']);


Route::get('instructors',[\App\Http\Controllers\FrontController::class,'instructors'])->name('instructors');
Route::get('instructors/{instructor}',[\App\Http\Controllers\FrontController::class,'instructorsShow'])->name('instructors.show');

Route::get('explore/{course}',[\App\Http\Controllers\FrontController::class,'showCourse'])->name('course.show');
Route::get('contact-us',[\App\Http\Controllers\FrontController::class,'contactUs'])->name('contact.us');
Route::get('about-us',[\App\Http\Controllers\FrontController::class,'aboutUs'])->name('about.us');
Route::get('branches',[\App\Http\Controllers\FrontController::class,'branches'])->name('branches');
Route::get('centers',[\App\Http\Controllers\FrontController::class,'centers'])->name('centers');
Route::get('blog/news',[\App\Http\Controllers\FrontController::class,'news'])->name('front.news');
Route::get('blog/news/{news}',[\App\Http\Controllers\FrontController::class,'newsShow'])->name('front.news.show');


Route::get('certificate/verify/{uuid?}',[\App\Http\Controllers\FrontController::class,'certificate_verify'])->name('front.certificate.verify');
Route::get('certificate/show/{uuid?}',[\App\Http\Controllers\FrontController::class,'certificate_show'])->name('front.certificate.show');


Route::post('/contact-us-form',[\App\Http\Controllers\FrontController::class,'submitContactUs'])->name('submitContactUs');
require __DIR__.'/auth.php';


Route::get('/cccc',function (){

    $certificates = \App\Models\Certificate::all();

    foreach ($certificates as $certificate){
        $uuid = Str::uuid()->toString();
        $certificate->update([
            'uuid' => $uuid,
        ]);
    }


//    $depars = \App\Models\Department::all();
//    foreach ($depars as $dep){
//        $c = json_decode($dep->courses,true);
//
//        foreach ($c['names'] as $index => $name){
//            $desc = $c['desc'][$index];
//            $course = \App\Models\Courses::create([
//                'name' => $name,
//                'info' => $desc,
//                'location' => ' ',
//                'dep_id' => $dep->id,
//                'price' => null,
//                'start_date' => null,
//                'cover' => '1670360646UK1.jpg',
//                'icon' => '',
//                'status' => 0,
//            ]);
//        }
//    }

});


