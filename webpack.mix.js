const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */



mix.sass('./resources/scss/know.scss','public/css');
mix.sass('./resources/scss/argon-dashboard.scss','public/css');
mix.js('./resources/js/know.js','public/js');
mix.js('./resources/js/argon-dashboard.js','public/js');

mix.copy('./resources/img/header-bg.webp','public/images/');
mix.options({
    processCssUrls: false // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
});

if (mix.inProduction()){
    mix.version();
}
