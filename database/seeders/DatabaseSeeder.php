<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        User::create([
            'name' => 'mohamad murad',
            'email' => 'mhdite7@gmail.com',
            'password' => Hash::make('12345678'),

        ]);

        User::create([
            'name' => 'nasrallah',
            'email' => 'knowhowacademya@gmail.com',
            'password' => Hash::make('Naser@1531980'),

        ]);
    }
}
