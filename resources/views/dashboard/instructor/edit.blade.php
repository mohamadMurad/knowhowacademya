@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="d-flex align-items-center">
                        <p class="mb-0">Edit Instructor</p>

                    </div>
                </div>
                <div class="card-body">
                    <form action="{{route('instructor.update',['instructor'=>$instructor->id])}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Name</label>
                                <input class="form-control" type="text" name="name" value="{{$instructor->name}}" onfocus="focused(this)" onfocusout="defocused(this)">
                                @if($errors->has('name'))
                                    <span class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Position</label>
                                <input class="form-control" type="text" name="position" value="{{$instructor->position}}" onfocus="focused(this)" onfocusout="defocused(this)">
                                @if($errors->has('position'))
                                    <span class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('position') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Info</label>
                                <textarea  class="form-control editor"  name="info">{{$instructor->info}}</textarea>
                                @if($errors->has('info'))
                                    <span class="text-danger text-gradient "> {{ $errors->first('info') }}</span>
                                @endif
                            </div>
                        </div>

                        @if(\Illuminate\Support\Facades\File::exists(public_path('instructor_image/'.$instructor->img)))
                            <div class="avatar avatar-xxl position-relative border-radius-lg shadow-sm overflow-hidden">
                                <img src="{{asset('instructor_image/'.$instructor->img)}}" alt="profile_image" class="img-fluid">
                            </div>
                            <span class="text-primary">if you want change this image please upload new one</span>
                        @endif

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">profile image</label>
                                <input type="file" class=" form-control custom-file-input" id="customFileLang"  name="img" onfocus="focused(this)" onfocusout="defocused(this)">
                                @if($errors->has('img'))
                                    <span class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('img') }}</span>
                                @endif
                            </div>
                        </div>



                        <div class="col-md-6">
                            <div class="form-group">
                               <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    </div>



@endsection
