@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="d-flex align-items-center">
                        <p class="mb-0">Add New Instructor</p>

                    </div>
                </div>
                <div class="card-body">
                    <form action="{{route('instructor.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Name</label>
                                <input class="form-control" type="text" name="name" value="{{old('name')}}" onfocus="focused(this)" onfocusout="defocused(this)">
                                @if($errors->has('name'))
                                    <span class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Position</label>
                                <input class="form-control" type="text" name="position" value="{{old('position')}}" onfocus="focused(this)" onfocusout="defocused(this)">
                                @if($errors->has('position'))
                                    <span class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('position') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Info</label>
                                <textarea  class="form-control editor"  name="info">{{old('info')}}</textarea>
                                @if($errors->has('info'))
                                    <span class="text-danger text-gradient "> {{ $errors->first('info') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">profile image</label>
                                <input type="file" class=" form-control custom-file-input" id="customFileLang"  name="img" onfocus="focused(this)" onfocusout="defocused(this)">
                                @if($errors->has('img'))
                                    <span class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('img') }}</span>
                                @endif
                            </div>
                        </div>



                        <div class="col-md-6">
                            <div class="form-group">
                               <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    </div>



@endsection
