@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="d-flex align-items-center">
                        <p class="mb-0">Add New Department</p>

                    </div>
                </div>
                <div class="card-body">
                    <form action="{{route('departments.update',['department'=>$department->id])}}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Name</label>
                                    <input class="form-control" type="text" name="name" value="{{$department->name}}"
                                          >
                                    @if($errors->has('name'))
                                        <span
                                            class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Description</label>
                                    <textarea class="form-control editor" name="desc"
                                             >{{$department->desc}}</textarea>
                                    @if($errors->has('desc'))
                                        <span class="text-danger text-gradient "> {{ $errors->first('desc') }}</span>
                                    @endif
                                </div>
                            </div>

{{--                            <div class="col-md-12">--}}


{{--                                <div class="form-group">--}}
{{--                                    <label for="example-text-input" class="form-control-label">Courses</label>--}}
{{--                                    <?php $coursess = json_decode($department->courses,true); ?>--}}
{{--                                    @if(isset($coursess['names']))--}}

{{--                                        @forelse($coursess['names'] as $index => $name)--}}

{{--                                            <div class="d-flex align-items-start gap-1 mb-2">--}}
{{--                                                <div style="width: 95%">--}}
{{--                                                    <input class="form-control mb-2" type="text" name="courses[names][]" required--}}
{{--                                                           value="{{$name}}"--}}
{{--                                                         placeholder="Course Name"--}}
{{--                                                    >--}}
{{--                                                    <textarea class="form-control" placeholder="Course Description" required--}}
{{--                                                              name="courses[desc][]">{{$coursess['desc'][$index]}}</textarea>--}}
{{--                                                </div>--}}

{{--                                                @if($index != 0)--}}
{{--                                                    <div class="input-group-append" style="width: 4%">--}}
{{--                                                        <button class="btn btn-outline-danger"  onclick="this.parentNode.parentNode.remove()" type="button" id="addCourse"><i--}}
{{--                                                                class="fa-solid fa-minus"></i></button>--}}
{{--                                                    </div>--}}
{{--                                                @else--}}
{{--                                                    <div class="input-group-append" style="width: 4%">--}}
{{--                                                        <button class="btn btn-outline-primary" type="button" id="addCourse"><i--}}
{{--                                                                class="fa-solid fa-plus"></i></button>--}}
{{--                                                    </div>--}}
{{--                                                @endif--}}


{{--                                            </div>--}}


{{--                                        @empty--}}
{{--                                            <div class="d-flex align-items-start gap-1 mb-2">--}}
{{--                                                <div style="width: 95%">--}}
{{--                                                    <input class="form-control mb-2" type="text" name="courses[names][]" required--}}
{{--                                                           value="{{isset(old('courses')['names'][0]) ? old('courses')['names'][0] : ''}}"--}}
{{--                                                          placeholder="Course Name"--}}
{{--                                                    >--}}
{{--                                                    <textarea class="form-control" placeholder="Course Description" required--}}
{{--                                                              name="courses[desc][]">{{isset(old('courses')['desc'][0]) ? old('courses')['desc'][0] : ''}}</textarea>--}}
{{--                                                </div>--}}

{{--                                                <div class="input-group-append" style="width: 4%">--}}
{{--                                                    <button class="btn btn-outline-primary" type="button" id="addCourse"><i--}}
{{--                                                            class="fa-solid fa-plus"></i></button>--}}
{{--                                                </div>--}}

{{--                                            </div>--}}
{{--                                        @endforelse--}}
{{--                                    @else--}}
{{--                                        <div class="d-flex align-items-start gap-1 mb-2">--}}
{{--                                            <div style="width: 95%">--}}
{{--                                                <input class="form-control mb-2" type="text" name="courses[names][]" required--}}
{{--                                                       value="{{isset(old('courses')['names'][0]) ? old('courses')['names'][0] : ''}}"--}}
{{--                                                       placeholder="Course Name"--}}
{{--                                                >--}}
{{--                                                <textarea class="form-control" placeholder="Course Description" required--}}
{{--                                                          name="courses[desc][]">{{isset(old('courses')['desc'][0]) ? old('courses')['desc'][0] : ''}}</textarea>--}}
{{--                                            </div>--}}

{{--                                            <div class="input-group-append" style="width: 4%">--}}
{{--                                                <button class="btn btn-outline-primary" type="button" id="addCourse"><i--}}
{{--                                                        class="fa-solid fa-plus"></i></button>--}}
{{--                                            </div>--}}

{{--                                        </div>--}}
{{--                                    @endif--}}



{{--                                    <div class="" id="other">--}}
{{--                                        @if(old('courses') != null && count(old('courses'))> 1)--}}
{{--                                            @foreach(old('courses') as $index=>$c)--}}
{{--                                                @if($index != 0)--}}
{{--                                                    <div class="d-flex align-items-start gap-1">--}}
{{--                                                        <input class="form-control" type="text" name="courses[]"--}}
{{--                                                               value="{{old('courses')[$index]}}"--}}
{{--                                                               onfocus="focused(this)"--}}
{{--                                                        >--}}
{{--                                                        <div class="input-group-append">--}}
{{--                                                            <button class="btn btn-outline-danger" type="button"--}}
{{--                                                                    id="addCourse"--}}
{{--                                                                    onclick="this.parentNode.parentNode.remove()"><i--}}
{{--                                                                    class="fa-solid fa-minus"></i></button>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                @endif--}}

{{--                                            @endforeach--}}
{{--                                        @endif--}}


{{--                                    </div>--}}

{{--                                    @if($errors->has('courses'))--}}
{{--                                        <span--}}
{{--                                            class="text-warning  text-gradient px-3 mb-0"> {{ $errors->first('courses') }}</span>--}}
{{--                                    @endif--}}
{{--                                </div>--}}
{{--                            </div>--}}

                            @if(\Illuminate\Support\Facades\File::exists(public_path('department_image/'.$department->image1)))
                                <div class="avatar avatar-xxl position-relative">
                                    <img src="{{asset('department_image/'.$department->image1)}}" alt="profile_image"
                                         class="w-100 border-radius-lg shadow-sm">
                                </div>
                                <span class="text-primary">if you want change this image please upload new one</span>
                            @endif
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label"> image1</label>
                                    <input type="file" class=" form-control custom-file-input" id="customFileLang"
                                           name="image1" >
                                    @if($errors->has('image1'))
                                        <span
                                            class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('image1') }}</span>
                                    @endif
                                </div>
                            </div>

                            {{--                            @if(\Illuminate\Support\Facades\File::exists(public_path('department_image/'.$department->image2)))--}}
                            {{--                                <div class="avatar avatar-xxl position-relative">--}}
                            {{--                                    <img src="{{asset('department_image/'.$department->image2)}}" alt="profile_image" class="w-100 border-radius-lg shadow-sm">--}}
                            {{--                                </div>--}}
                            {{--                                <span class="text-primary">if you want change this image please upload new one</span>--}}
                            {{--                            @endif--}}
                            {{--                            <div class="col-md-12">--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label for="example-text-input" class="form-control-label"> image2</label>--}}
                            {{--                                    <input type="file" class=" form-control custom-file-input" id="customFileLang"  name="image2" onfocus="focused(this)" onfocusout="defocused(this)">--}}
                            {{--                                    @if($errors->has('image2'))--}}
                            {{--                                        <span class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('image2') }}</span>--}}
                            {{--                                    @endif--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}

                            {{--                            @if(\Illuminate\Support\Facades\File::exists(public_path('department_image/'.$department->image3)))--}}
                            {{--                                <div class="avatar avatar-xxl position-relative">--}}
                            {{--                                    <img src="{{asset('department_image/'.$department->image3)}}" alt="profile_image" class="w-100 border-radius-lg shadow-sm">--}}
                            {{--                                </div>--}}
                            {{--                                <span class="text-primary">if you want change this image please upload new one</span>--}}
                            {{--                            @endif--}}
                            {{--                            <div class="col-md-12">--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label for="example-text-input" class="form-control-label"> image3</label>--}}
                            {{--                                    <input type="file" class=" form-control custom-file-input" id="customFileLang"  name="image3" onfocus="focused(this)" onfocusout="defocused(this)">--}}
                            {{--                                    @if($errors->has('image3'))--}}
                            {{--                                        <span class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('image3') }}</span>--}}
                            {{--                                    @endif--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}

                            {{--                            @if(\Illuminate\Support\Facades\File::exists(public_path('department_image/'.$department->image4)))--}}
                            {{--                                <div class="avatar avatar-xxl position-relative">--}}
                            {{--                                    <img src="{{asset('department_image/'.$department->image4)}}" alt="profile_image" class="w-100 border-radius-lg shadow-sm">--}}
                            {{--                                </div>--}}
                            {{--                                <span class="text-primary">if you want change this image please upload new one</span>--}}
                            {{--                            @endif--}}
                            {{--                            <div class="col-md-12">--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label for="example-text-input" class="form-control-label"> image4</label>--}}
                            {{--                                    <input type="file" class=" form-control custom-file-input" id="customFileLang"  name="image4" onfocus="focused(this)" onfocusout="defocused(this)">--}}
                            {{--                                    @if($errors->has('image4'))--}}
                            {{--                                        <span class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('image4') }}</span>--}}
                            {{--                                    @endif--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}

                            <div class="col-md-6">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-info">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>



@endsection

@push('script')

    <script>
        let $count = 0;

        document.getElementById('addCourse').addEventListener("click", function () {
            $count++;
            /*   let inputs = '<div class="d-flex align-items-start gap-1" id="d_' + $count + '">       ' +
                   '     <input class="form-control" type="text" name="courses[]" value="" onfocus="focused(this)" onfocusout="defocused(this)">' +
                   ' <div class="input-group-append"> ' +
                   '<button class="btn btn-outline-danger" type="button" id="delete_me" onclick="this.parentNode.parentNode.remove()">' +
                   '<i class="fa-solid fa-minus"></i>' +
                   '</button>' +
                   ' </div> </div>';*/

            let inputs = '<div class="d-flex align-items-start gap-1 mb-2"  id="d_' + $count + '">' +
                '<div style="width: 95%">' +
                '<input class="form-control mb-2" type="text" name="courses[names][]" required' +
                ' value=""  placeholder="Course Name">' +
                '<textarea class="form-control" placeholder="Course Description" name="courses[desc][]"  required></textarea>' +
                ' </div>' +
                '<div class="input-group-append" style="width: 4%">' +
                '<button class="btn btn-outline-danger" type="button" id="delete_me" onclick="this.parentNode.parentNode.remove()">' +
                '<i  class="fa-solid fa-minus"></i></button>   </div> </div>'

            document.getElementById('other').insertAdjacentHTML('beforeend', inputs)
        });


    </script>
@endpush

