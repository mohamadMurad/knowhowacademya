@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="d-flex align-items-center">
                        <p class="mb-0">Add New Branch</p>

                    </div>
                </div>
                <div class="card-body">
                    <form action="{{route('branches.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                    <div class="row">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Country</label>
                                <input class="form-control" type="text" name="city" value="{{old('city')}}" onfocus="focused(this)" onfocusout="defocused(this)">
                                @if($errors->has('city'))
                                    <span class="text-red-100  text-gradient px-3 mb-0"> {{ $errors->first('city') }}</span>
                                @endif
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">City</label>
                                <input class="form-control" type="text" name="name" value="{{old('name')}}" onfocus="focused(this)" onfocusout="defocused(this)">
                                @if($errors->has('name'))
                                    <span class="text-red-100  text-gradient px-3 mb-0"> {{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Address</label>
                                <input class="form-control" type="text" name="address" value="{{old('address')}}" onfocus="focused(this)" onfocusout="defocused(this)">
                                @if($errors->has('address'))
                                    <span class="text-red-100  text-gradient px-3 mb-0"> {{ $errors->first('address') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Agent</label>
                                <input class="form-control" type="text" name="agent" value="{{old('agent')}}" >
                                @if($errors->has('agent'))
                                    <span class="text-red-100  text-gradient px-3 mb-0"> {{ $errors->first('agent') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Phone</label>
                                <input class="form-control" type="tel" name="phone" value="{{old('phone')}}" onfocus="focused(this)" onfocusout="defocused(this)">
                                @if($errors->has('phone'))
                                    <span class="text-red-100  text-gradient px-3 mb-0"> {{ $errors->first('phone') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                               <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    </div>



@endsection
