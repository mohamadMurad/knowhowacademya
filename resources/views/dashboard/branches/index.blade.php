@extends('layouts.app')

@section('content')



    <div class="row">
        <div class="col-12">
            <div class="card mb-4">

                <div class="card-header pb-0">
                    <h6>Branches</h6>
                    <a href="{{route('branches.create')}}" class="btn btn-primary btn-sm ms-auto"> Add </a>
                </div>
                @include('dashboard.shared.alerts')
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0">
                            <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Country
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">City
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Address
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Agent
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    phone
                                </th>

                                <th class="text-secondary opacity-7"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($branches as $cert)

                                <tr>
                                    <td style="overflow: hidden;max-width: 100px;">
                                        <p class="text-xs font-weight-bold mb-0">{{$cert->city}}</p>

                                    </td>
                                    <td>
                                        <div class="d-flex px-2 py-1">

                                            <div class="d-flex flex-column justify-content-center">
                                                <h6 class="mb-0 text-sm">{{$cert->name}}</h6>

                                            </div>
                                        </div>
                                    </td>
                                    <td style="overflow: hidden;max-width: 100px;">
                                        <p class="text-xs font-weight-bold mb-0">{{$cert->address}}</p>

                                    </td>
                                    <td style="overflow: hidden;max-width: 100px;">
                                        <p class="text-xs font-weight-bold mb-0">{{$cert->agent}}</p>

                                    </td>
                                    <td style="overflow: hidden;max-width: 100px;">
                                        <p class="text-xs font-weight-bold mb-0">{{$cert->phone}}</p>

                                    </td>
                                    <td class="align-middle">
                                        <form action="{{route('branches.destroy',['branch'=>$cert->id])}}"
                                              id="delete_{{$cert->id}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <a href="{{route('branches.edit',['branch'=>$cert->id])}}"
                                               class=" text-success  font-weight-bold text-xs">
                                                <i class="fa-solid fa-pen-alt me-2" aria-hidden="true"></i>
                                                Edit
                                            </a>
                                            <a href="javascript:deleteForm('delete_{{$cert->id}}');"
                                               class=" text-warning  font-weight-bold text-xs">
                                                <i class="fa-solid fa-trash-alt ms-2" aria-hidden="true"></i>
                                                Delete
                                            </a>

                                        </form>
                                    </td>

                                </tr>



                            @empty
                                <tr>

                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">No Data</p>

                                    </td>


                                </tr>

                            @endforelse

                            </tbody>
                        </table>

                        <div class="text-center m-3">
                            {{$branches->links()}}
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
