@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="d-flex align-items-center">
                        <p class="mb-0">Site Settings</p>

                    </div>
                </div>
                <div class="card-body">


                    <form action="{{route('settings.update')}}" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="nav-wrapper position-relative end-0">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab"
                                            data-bs-target="#general" type="button" role="tab" aria-controls="home"
                                            aria-selected="true">General
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="profile-tab" data-bs-toggle="tab"
                                            data-bs-target="#social" type="button" role="tab" aria-controls="profile"
                                            aria-selected="false">Social
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="contact-tab" data-bs-toggle="tab"
                                            data-bs-target="#contact" type="button" role="tab" aria-controls="contact"
                                            aria-selected="false">Contact
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="contact-tab" data-bs-toggle="tab"
                                            data-bs-target="#pages" type="button" role="tab" aria-controls="contact"
                                            aria-selected="false">Pages
                                    </button>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="general" role="tabpanel"
                                     aria-labelledby="profile-tab">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Site
                                                keywords</label>
                                            <input class="form-control" type="text" required name="site_keywords"
                                                   value="{{$siteKeywords}}" onfocus="focused(this)"
                                                   onfocusout="defocused(this)">
                                            <span class="text-xs">separated by commas , ex: courses,training</span>
                                            @if($errors->has('site_keywords'))
                                                <span
                                                    class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('site_keywords') }}</span>
                                            @endif

                                        </div>
                                    </div>


                                    @if(\Illuminate\Support\Facades\File::exists(public_path('header_image/'.$header_img)))
                                        <div class="avatar avatar-xxl position-relative">
                                            <img src="{{asset('header_image/'.$header_img)}}" alt="profile_image"
                                                 class="w-100 border-radius-lg shadow-sm">
                                        </div>
                                        <span
                                            class="text-primary">if you want change this image please upload new one</span>
                                    @endif
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label"> Header
                                                Photo</label>
                                            <input type="file" class=" form-control custom-file-input"
                                                   id="customFileLang"
                                                   name="header_img">
                                            @if($errors->has('header_img'))
                                                <span
                                                    class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('header_img') }}</span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Site
                                                Description</label>
                                            <textarea class="form-control editor"
                                                      name="site_desc">{{$siteDescription}}</textarea>
                                            @if($errors->has('site_desc'))
                                                <span
                                                    class="text-danger text-gradient "> {{ $errors->first('site_desc') }}</span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Contact US
                                                Description</label>
                                            <textarea class="form-control editor"
                                                      name="contactUsDesc">{{$contactUsDesc}}</textarea>
                                            @if($errors->has('contactUsDesc'))
                                                <span
                                                    class="text-danger text-gradient "> {{ $errors->first('contactUsDesc') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane fade " id="contact" role="tabpanel" aria-labelledby="home-tab">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Whatsapp
                                                Number</label>
                                            <input class="form-control" required type="text" name="whatsapp_number"
                                                   value="{{$whatsappNumber}}" onfocus="focused(this)"
                                                   onfocusout="defocused(this)">
                                            @if($errors->has('whatsapp_number'))
                                                <span
                                                    class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('whatsapp_number') }}</span>
                                            @endif
                                            <span class="text-xs">Number must start with country code ex: +963</span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Contact
                                                Email</label>
                                            <input class="form-control" required type="email" name="contact_email"
                                                   value="{{$contactEmail}}" onfocus="focused(this)"
                                                   onfocusout="defocused(this)">
                                            @if($errors->has('contact_email'))
                                                <span
                                                    class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('contact_email') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Contact Us
                                                Email</label>
                                            <input class="form-control" type="email" required name="contactUsEmail"
                                                   value="{{$contactUsEmail}}" onfocus="focused(this)"
                                                   onfocusout="defocused(this)">
                                            @if($errors->has('contactUsEmail'))
                                                <span
                                                    class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('contactUsEmail') }}</span>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Contact
                                                Phone</label>
                                            <input class="form-control" required type="text" name="contact_phone"
                                                   value="{{$contactPhone}}" onfocus="focused(this)"
                                                   onfocusout="defocused(this)">
                                            @if($errors->has('contact_phone'))
                                                <span
                                                    class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('contact_phone') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Address 1</label>
                                            <input class="form-control" type="text" required name="address1"
                                                   value="{{$address1}}" onfocus="focused(this)"
                                                   onfocusout="defocused(this)">
                                            @if($errors->has('address1'))
                                                <span
                                                    class="text-danger text-gradient "> {{ $errors->first('address1') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Address 2</label>
                                            <input class="form-control" type="text" required name="address2"
                                                   value="{{$address2}}" onfocus="focused(this)"
                                                   onfocusout="defocused(this)">
                                            @if($errors->has('address2'))
                                                <span
                                                    class="text-danger text-gradient "> {{ $errors->first('address2') }}</span>
                                            @endif
                                        </div>
                                    </div>


                                </div>
                                <div class="tab-pane fade" id="social" role="tabpanel" aria-labelledby="contact-tab">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Facebook Page
                                                Name</label>
                                            <input class="form-control" type="text" required name="facebookName"
                                                   value="{{$siteFacebookName}}" onfocus="focused(this)"
                                                   onfocusout="defocused(this)">
                                            @if($errors->has('facebookName'))
                                                <span
                                                    class="text-danger text-gradient "> {{ $errors->first('facebookName') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Facebook Page
                                                Link</label>
                                            <input class="form-control" type="text" required name="facebook"
                                                   value="{{$siteFacebook}}" onfocus="focused(this)"
                                                   onfocusout="defocused(this)">
                                            @if($errors->has('facebook'))
                                                <span
                                                    class="text-danger text-gradient "> {{ $errors->first('facebook') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Instagram
                                                Name</label>
                                            <input class="form-control" type="text" required name="instagramName"
                                                   value="{{$siteInstaName}}" onfocus="focused(this)"
                                                   onfocusout="defocused(this)">
                                            @if($errors->has('instagramName'))
                                                <span
                                                    class="text-danger text-gradient "> {{ $errors->first('instagramName') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">Instagram
                                                Link</label>
                                            <input class="form-control" type="text" required name="instagram"
                                                   value="{{$siteInsta}}" onfocus="focused(this)"
                                                   onfocusout="defocused(this)">
                                            @if($errors->has('instagram'))
                                                <span
                                                    class="text-danger text-gradient "> {{ $errors->first('instagram') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input1" class="form-control-label">LinkedIn
                                                Name</label>
                                            <input class="form-control" type="text"  name="LinkedinName"
                                                   value="{{$LinkedinName}}"
                                                  >
                                            @if($errors->has('LinkedinName'))
                                                <span
                                                    class="text-danger text-gradient "> {{ $errors->first('LinkedinName') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">LinkedIn
                                                Link</label>
                                            <input class="form-control" type="text"  name="Linkedin"
                                                   value="{{$Linkedin}}"
                                                  >
                                            @if($errors->has('Linkedin'))
                                                <span
                                                    class="text-danger text-gradient "> {{ $errors->first('Linkedin') }}</span>
                                            @endif
                                        </div>
                                    </div>


                                </div>
                                <div class="tab-pane fade" id="pages" role="tabpanel" aria-labelledby="contact-tab">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="example-text-input" class="form-control-label">About US</label>
                                            <textarea id="aboutUs" rows="6" class="form-control editor"
                                                      name="aboutUs">{{$aboutUs}}</textarea>
                                            @if($errors->has('aboutUs'))
                                                <span
                                                    class="text-danger text-gradient "> {{ $errors->first('aboutUs') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">


                            {{--                            --}}
                            {{--                            <div class="col-md-12">--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label for="example-text-input"  class="form-control-label">Site robots_txt</label>--}}
                            {{--                                    <textarea  class="form-control" required name="robots_txt" onfocus="focused(this)" onfocusout="defocused(this)">{{$robots_txt}}</textarea>--}}
                            {{--                                    @if($errors->has('robots_txt'))--}}
                            {{--                                        <span class="text-danger text-gradient "> {{ $errors->first('robots_txt') }}</span>--}}
                            {{--                                    @endif--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}


                            <div class="col-md-6">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection
