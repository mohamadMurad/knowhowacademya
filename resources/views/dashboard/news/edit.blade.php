@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="d-flex align-items-center">
                        <p class="mb-0">Edit News</p>

                    </div>
                </div>
                <div class="card-body">
                    <form action="{{route('news.update',$news)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Title</label>
                                    <input class="form-control" type="text" name="title" value="{{old('title',$news->title)}}" required
                                           onfocus="focused(this)" onfocusout="defocused(this)">
                                    @if($errors->has('title'))
                                        <span
                                            class="text-red-100  text-gradient px-3 mb-0"> {{ $errors->first('title') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Cover</label>
                                    <input class="form-control" type="file" accept="image/*" name="cover" value="{{old('cover')}}" required
                                           onfocus="focused(this)" onfocusout="defocused(this)">
                                    <div class="col-12 p-2">
                                        <img src="{{$news->cover}}"  style="width:100px;max-height: 100px;">
                                    </div>
                                    @if($errors->has('cover'))
                                        <span
                                            class="text-red-100  text-gradient px-3 mb-0"> {{ $errors->first('cover') }}</span>
                                    @endif
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Content</label>
                                    <textarea id="aboutUs" rows="6" class="form-control editor"
                                              name="content">{{old('content',$news->body)}}</textarea>
                                    @if($errors->has('content'))
                                        <span
                                            class="text-danger text-gradient "> {{ $errors->first('content') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-info">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>



@endsection
