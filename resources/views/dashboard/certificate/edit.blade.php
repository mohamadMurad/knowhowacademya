@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="d-flex align-items-center">
                        <p class="mb-0">Edit Certificate</p>

                    </div>
                </div>
                <div class="card-body">
                    <form action="{{route('certificates.update',['certificate'=>$certificate->id])}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Certificate ID</label>
                                    <input class="form-control" type="text" name="cert_id"
                                           value="{{$certificate->cert_id}}"
                                           onfocus="focused(this)" onfocusout="defocused(this)">
                                    @if($errors->has('cert_id'))
                                        <span
                                            class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('cert_id') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Certificate File</label>
                                    <input class="form-control" type="file" accept="application/pdf" name="file"
                                           required>
                                    <small>Dont upload new one if you not need to update it</small>
                                    @error('file')

                                    <span
                                        class="text-danger  text-gradient px-3 mb-0"> {{$message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Info</label>
                                    <textarea class="form-control editor" name="info"
                                    >{{$certificate->info}}</textarea>
                                    @if($errors->has('info'))
                                        <span class="text-danger text-gradient "> {{ $errors->first('info') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Add</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection
