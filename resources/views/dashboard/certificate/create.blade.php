@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="d-flex align-items-center">
                        <p class="mb-0">Add New Certificate</p>

                    </div>
                </div>
                <div class="card-body">
                    <form action="{{route('certificates.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Certificate ID</label>
                                    <input class="form-control" type="text" name="cert_id" value="{{old('cert_id')}}"

                                    >
                                    @error('cert_id')
                                    <span
                                        class="text-danger text-gradient px-3 mb-0"> {{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Certificate File</label>
                                    <input class="form-control" type="file" accept="application/pdf" name="file"
                                           required>
                                    @error('file')

                                    <span
                                        class="text-danger  text-gradient px-3 mb-0"> {{$message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Info</label>
                                    <textarea class="editor" name="info">{{old('info')}}</textarea>
                                    @error('info')

                                    <span
                                        class="text-danger  text-gradient px-3 mb-0"> {{$message }}</span>
                                    @enderror

                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Add</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection
