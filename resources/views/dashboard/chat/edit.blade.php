@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="d-flex align-items-center">
                        <p class="mb-0">Edit chat</p>

                    </div>
                </div>
                <div class="card-body">
                    <form action="{{route('chats.update',['chat'=>$chat->id])}}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">question</label>
                                    <input class="form-control" type="text" name="question" value="{{$chat->question}}"
                                           onfocus="focused(this)" onfocusout="defocused(this)">
                                    @if($errors->has('question'))
                                        <span
                                            class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('question') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Answer</label>
                                    <textarea class="form-control" name="answer"
                                             >{{$chat->answer}}</textarea>
                                    @if($errors->has('answer'))
                                        <span class="text-danger text-gradient "> {{ $errors->first('answer') }}</span>
                                    @endif
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>



@endsection
