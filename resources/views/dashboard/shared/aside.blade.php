<aside class="sidenav bg-white navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-4 "
       id="sidenav-main">
    <div class="sidenav-header">
        <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none"
           aria-hidden="true" id="iconSidenav"></i>
        <a class="navbar-brand m-0" href="{{route('dashboard')}}"
           target="_blank">
            <img src="{{asset('img/footer-logo.svg')}}" class="navbar-brand-img h-100" alt="main_logo">
            <span class="ms-1 font-weight-bold">{{ config('app.name', 'Laravel') }}</span>
        </a>
    </div>
    <hr class="horizontal dark mt-0">
    <div class="collapse navbar-collapse  w-auto " id="sidenav-collapse-main">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link {{Route::is('dashboard') ? 'active':''}}" href="{{route('dashboard')}}">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="fa-solid fa-dashboard text-primary text-sm opacity-10"></i>
                    </div>
                    <span class="nav-link-text ms-1">Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::is('certificates.*') ? 'active':''}}"
                   href="{{route('certificates.index')}}">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="fa-solid fa-certificate text-warning text-sm opacity-10"></i>
                    </div>
                    <span class="nav-link-text ms-1">Certificates</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::is('news.*') ? 'active':''}}"
                   href="{{route('news.index')}}">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="fa-solid fa-newspaper text-warning text-sm opacity-10"></i>
                    </div>
                    <span class="nav-link-text ms-1">News</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::is('departments.*') ? 'active':''}}"
                   href="{{route('departments.index')}}">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">

                        <i class="fa-brands fa-delicious text-success text-sm opacity-10"></i>

                    </div>
                    <span class="nav-link-text ms-1">Departments</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::is('accreditCenter.*') ? 'active':''}}"
                   href="{{route('accreditCenter.index')}}">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">

                        <i class="fa-brands fa-delicious text-success text-sm opacity-10"></i>

                    </div>
                    <span class="nav-link-text ms-1">Accredit Center</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::is('courses.*') ? 'active':''}}" href="{{route('courses.index')}}">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">

                        <i class="fa-solid fa-graduation-cap text-primary text-sm opacity-10"></i>

                    </div>
                    <span class="nav-link-text ms-1">Courses</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::is('chats.*') ? 'active':''}}" href="{{route('chats.index')}}">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">

                        <i class="fa-solid fa-message text-primary text-sm opacity-10"></i>

                    </div>
                    <span class="nav-link-text ms-1">Chats</span>
                </a>
            </li>

{{--            <li class="nav-item">--}}
{{--                <a class="nav-link {{Route::is('instructor.*') ? 'active':''}}" href="{{route('instructor.index')}}">--}}
{{--                    <div--}}
{{--                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">--}}

{{--                        <i class="fa-solid fa-people-group text-primary text-sm opacity-10"></i>--}}

{{--                    </div>--}}
{{--                    <span class="nav-link-text ms-1">Instructor</span>--}}
{{--                </a>--}}
{{--            </li>--}}

            <li class="nav-item">
                <a class="nav-link {{Route::is('branches.*') ? 'active':''}}" href="{{route('branches.index')}}">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">

                        <i class="fa-solid fa-location text-primary text-sm opacity-10"></i>

                    </div>
                    <span class="nav-link-text ms-1">Branches</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::is('settings.*') ? 'active':''}}" href="{{route('settings.index')}}">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">

                        <i class="fa-solid fa-gear text-primary text-sm opacity-10"></i>

                    </div>
                    <span class="nav-link-text ms-1">Settings</span>
                </a>
            </li>

        </ul>
    </div>

</aside>
