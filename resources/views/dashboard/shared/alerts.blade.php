@if(session()->has('success'))

    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <span class="alert-icon"><i class="ni ni-like-2"></i></span>
        <span class="alert-text">{{session()->get('success')}}</span>
    </div>

@endif

@if(session()->has('error'))

    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <span class="alert-icon"><i class="ni ni-like-2"></i></span>
        <span class="alert-text">{{session()->get('error')}}</span>
    </div>

@endif
