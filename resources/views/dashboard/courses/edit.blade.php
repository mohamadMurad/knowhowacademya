@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="d-flex align-items-center">
                        <p class="mb-0">Edit Course</p>

                    </div>
                </div>
                <div class="card-body">
                    <form action="{{route('courses.update',['course' => $course->id])}}" method="post" enctype="multipart/form-data">
                        @method('put')
                        @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label"> Department</label>
                                <select class="form-control" name="dep_id">
                                    <option value="null" disabled selected>Select Department</option>
                                    @foreach($departments as $dept)
                                        <option value="{{$dept->id}}" {{$dept->id == $course->department->id ? 'selected' : ''}}>{{$dept->name}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('dep_id'))
                                    <span class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('dep_id') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Name</label>
                                <input class="form-control" type="text" name="name" value="{{old('name',$course->name)}}" onfocus="focused(this)" onfocusout="defocused(this)">
                                @if($errors->has('name'))
                                    <span class="text-danger text-gradient "> {{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Start Date</label>
                                <input class="form-control" type="date" name="start_date"
                                       value="{{old('start_date',$course->start_date)}}">
                                @if($errors->has('start_date'))
                                    <span class="text-danger text-gradient "> {{ $errors->first('start_date') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Location</label>
                                <input class="form-control" type="text" name="location" value="{{old('location',$course->location)}}" onfocus="focused(this)" onfocusout="defocused(this)">
                                @if($errors->has('location'))
                                    <span class="text-danger text-gradient "> {{ $errors->first('location') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label"> Price</label>
                                <input class="form-control" type="text" min="1" name="price" value="{{old('price',$course->price)}}" onfocus="focused(this)" onfocusout="defocused(this)">
                                @if($errors->has('price'))
                                    <span class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('price') }}</span>
                                @endif
                            </div>
                        </div>


                        @if(\Illuminate\Support\Facades\Storage::disk('public')->exists($course->cover))
                            <div class="avatar avatar-xxl position-relative">
                                <img src="{{\Illuminate\Support\Facades\Storage::disk('public')->url($course->cover)}}" alt="profile_image" class="w-100 border-radius-lg shadow-sm">
                            </div>
                            <span class="text-primary">if you want change this image please upload new one</span>
                        @endif
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label"> Cover</label>
                                <input type="file" class=" form-control custom-file-input" id="customFileLang"  name="cover" onfocus="focused(this)" onfocusout="defocused(this)">
                                @if($errors->has('cover'))
                                    <span class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('cover') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Description</label>
                                <textarea  class="form-control editor"  name="info" >{{old('info',$course->info)}}</textarea>
                                @if($errors->has('info'))
                                    <span class="text-danger text-gradient "> {{ $errors->first('info') }}</span>
                                @endif
                            </div>
                        </div>

{{--                        @if(\Illuminate\Support\Facades\File::exists(public_path('courses_cover/'.$course->icon)))--}}
{{--                            <div class="avatar avatar-xxl position-relative">--}}
{{--                                <img src="{{asset('courses_cover/'.$course->icon)}}" alt="profile_image" class="w-100 border-radius-lg shadow-sm">--}}
{{--                            </div>--}}
{{--                            <span class="text-primary">if you want change this image please upload new one</span>--}}
{{--                        @endif--}}
{{--                        <div class="col-md-12">--}}
{{--                            <div class="form-group">--}}
{{--                                <label for="example-text-input" class="form-control-label"> Icon</label>--}}
{{--                                <input type="file" class=" form-control custom-file-input" id="customFileLang"  name="icon" onfocus="focused(this)" onfocusout="defocused(this)">--}}
{{--                                @if($errors->has('icon'))--}}
{{--                                    <span class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('icon') }}</span>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}

                        <div class="col-md-6">
                            <div class="form-group">
                               <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    </div>



@endsection
