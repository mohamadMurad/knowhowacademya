@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="d-flex align-items-center">
                        <p class="mb-0">Add New Department</p>

                    </div>
                </div>
                <div class="card-body">
                    <form action="{{route('accreditCenter.update',['accreditCenter'=>$accreditCenter->id])}}"
                          method="post"
                          enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Name</label>
                                    <input class="form-control" type="text" name="name"
                                           value="{{$accreditCenter->name}}"
                                    >
                                    @if($errors->has('name'))
                                        <span
                                                class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Phone</label>
                                    <input class="form-control" type="text" name="phone"
                                           value="{{$accreditCenter->phone}}"
                                    >
                                    @if($errors->has('phone'))
                                        <span
                                                class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('phone') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Address</label>
                                    <textarea class="form-control editor" name="address"
                                    >{{$accreditCenter->address}}</textarea>
                                    @if($errors->has('address'))
                                        <span class="text-danger text-gradient "> {{ $errors->first('address') }}</span>
                                    @endif
                                </div>
                            </div>
                            @if($accreditCenter->hasMedia('accreditCenter'))
                                <div class="avatar avatar-xxl position-relative">
                                    <img src="{{$accreditCenter->getFirstMediaUrl('accreditCenter')}}"
                                         alt="profile_image"
                                         class="w-100 border-radius-lg shadow-sm">
                                </div>
                                <span class="text-primary">if you want change this image please upload new one</span>
                            @endif
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label"> image1</label>
                                    <input type="file" class=" form-control custom-file-input" id="customFileLang"
                                           name="image1">
                                    @if($errors->has('image1'))
                                        <span
                                                class="text-danger text-gradient px-3 mb-0"> {{ $errors->first('image1') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-info">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection

@push('script')

    <script>
        let $count = 0;

        document.getElementById('addCourse').addEventListener("click", function () {
            $count++;
            /*   let inputs = '<div class="d-flex align-items-start gap-1" id="d_' + $count + '">       ' +
                   '     <input class="form-control" type="text" name="courses[]" value="" onfocus="focused(this)" onfocusout="defocused(this)">' +
                   ' <div class="input-group-append"> ' +
                   '<button class="btn btn-outline-danger" type="button" id="delete_me" onclick="this.parentNode.parentNode.remove()">' +
                   '<i class="fa-solid fa-minus"></i>' +
                   '</button>' +
                   ' </div> </div>';*/

            let inputs = '<div class="d-flex align-items-start gap-1 mb-2"  id="d_' + $count + '">' +
                '<div style="width: 95%">' +
                '<input class="form-control mb-2" type="text" name="courses[names][]" required' +
                ' value=""  placeholder="Course Name">' +
                '<textarea class="form-control" placeholder="Course Description" name="courses[desc][]"  required></textarea>' +
                ' </div>' +
                '<div class="input-group-append" style="width: 4%">' +
                '<button class="btn btn-outline-danger" type="button" id="delete_me" onclick="this.parentNode.parentNode.remove()">' +
                '<i  class="fa-solid fa-minus"></i></button>   </div> </div>'

            document.getElementById('other').insertAdjacentHTML('beforeend', inputs)
        });


    </script>
@endpush

