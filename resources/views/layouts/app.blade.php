<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/argon-dashboard.css') }}">

    <link href='https://cdn.jsdelivr.net/npm/froala-editor@latest/css/froala_editor.pkgd.min.css' rel='stylesheet'
          type='text/css'/>


    {{--    <link rel="stylesheet" href="{{ asset('css/nucleo-icons.css') }}">--}}
    {{--    <link rel="stylesheet" href="{{ asset('css/nucleo-svg.css') }}">--}}
    <!-- Font Awesome Icons -->
    {{--        <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>--}}
    <!-- Scripts -->
    {{--        <script src="{{ mix('js/app.js') }}" defer></script>--}}
</head>
<body class="g-sidenav-show   bg-gray-100">

<div class="min-height-300 bg-primary position-absolute w-100"></div>
@include('dashboard.shared.aside')
<main class="main-content position-relative border-radius-lg ps">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur"
         data-scroll="false">
        <div class="container-fluid py-1 px-3">
            {{--            <nav aria-label="breadcrumb">--}}
            {{--                <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">--}}
            {{--                    <li class="breadcrumb-item text-sm text-white"><a class="opacity-5 text-white" href="javascript:;">Pages</a>--}}
            {{--                    </li>--}}
            {{--                    <li class="breadcrumb-item text-sm text-white active" aria-current="page">Tables</li>--}}
            {{--                </ol>--}}
            {{--                <h6 class="font-weight-bolder text-white mb-0">Tables</h6>--}}
            {{--            </nav>--}}
            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">

                <ul class="navbar-nav  justify-content-end">
                    <li class="nav-item d-flex align-items-center">
                        <div class="me-4  text-white font-weight-bold px">{{ Auth::user()->name }}</div>
                        <form method="POST" action="{{ route('logout') }}" id="sign_out_form">
                            @csrf
                            <a href="javascript:;document.getElementById('sign_out_form').submit();"
                               class="nav-link text-white font-weight-bold px-0">
                                <i class="fa fa-sign-out me-sm-1" aria-hidden="true"></i>
                                <span class="d-sm-inline d-none">Sign out</span>
                            </a>
                        </form>

                    </li>

                </ul>
            </div>

        </div>
    </nav>
    <!-- End Navbar -->
    <div class="container-fluid py-4">

        @yield('content')

        <footer class="footer pt-3  ">
            <div class="container-fluid">
                <div class="row align-items-center justify-content-lg-between">
                    <div class="col-lg-6 mb-lg-0 mb-4">
                        <div class="copyright text-center text-sm text-muted text-lg-start">
                            ©
                            <script>
                                document.write(new Date().getFullYear())
                            </script>

                            made by
                            <a href="https://www.linkedin.com/in/mohamadmurad/" class="font-weight-bold"
                               target="_blank">Mohamad Murad</a>
                            for a better web.
                        </div>
                    </div>

                </div>
            </div>
        </footer>
    </div>
</main>

<!--   Core JS Files   -->
<script src="{{ asset('js/core/popper.min.js') }}"></script>
<script src="{{ asset('js/core/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/plugins/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('js/plugins/smooth-scrollbar.min.js') }}"></script>
<script src="{{ mix('js/argon-dashboard.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/29.2.0/classic/ckeditor.js"></script>

<script>
    // var editor = new FroalaEditor('.editor', {
    //     toolbarInline: false,
    //     fileInsertButtons: [],
    //     fileUpload: false,
    //
    //     imageUpload: false,
    //     pasteImage: false,
    //     quickInsertEnabled: false,
    //     // Set custom buttons with separator between them.
    //     toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|',
    //         'fontFamily', 'fontSize', 'color', 'inlineClass', 'inlineStyle', 'paragraphStyle', 'lineHeight', '|',
    //         'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-',
    //         'insertLink', 'embedly', 'insertTable', '|', 'emoticons', 'fontAwesome', 'specialCharacters',
    //         'insertHR', 'selectAll', 'clearFormatting', '|', 'spellChecker', '|', 'undo', 'redo'],
    //     quickInsertButtons: []
    // });
    // var allEditors = document.querySelectorAll('.editor');
    // for (var i = 0; i < allEditors.length; ++i) {
    //
    //     ClassicEditor
    //         .create(allEditors[i], {
    //             removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
    //         })
    //     ;
    // }


    function deleteForm(id) {


        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                document.getElementById(id).submit();
            }
        })
    }


</script>
<script type="text/javascript" src="{{asset('/js/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/tinymce/ar.js')}}"></script>


<script>
    @if(auth()->check())
    tinymce.init({
        selector: '.editor,#editor',
        plugins: ' advlist image media autolink code codesample directionality table wordcount quickbars link lists ',
        toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment',
        contextmenu: 'link image imagetools table configurepermanentpen',
        relative_urls: false,
        images_upload_url: "{{route('upload.image',['_token' => csrf_token() ])}}",
        file_picker_types: 'file image media',
        image_caption: true,
        image_dimensions: true,
        // directionality : 'ltr',
        // language:'ar',
        quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
        // quickbars_selection_toolbar: 'bold italic |h1 h2 h3 h4 h5 h6| formatselect | quicklink blockquote | numlist bullist',
        entity_encoding: "raw",
        verify_html: false,
        object_resizing: 'img',
    });

    @else
    /* Guest Js */


    @endif
</script>
@stack('script')


</body>
</html>
