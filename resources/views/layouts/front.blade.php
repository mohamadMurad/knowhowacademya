<!--
=========================================================
* Know How Training Academya - v1.0.0
=========================================================

* Product Page: https://www.linkedin.com/in/mohamadmurad/
* Copyright 2022 Mohamad Murad (https://www.linkedin.com/in/mohamadmurad)
 Coded by Mohamad Murad

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="16x16" href="{{asset('favicon-16x16.png')}}">
    <link rel="icon" type="image/x-icon" href="{{asset('favicon.ico')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('favicon.ico')}}">


    <meta name="keywords" content="{{$siteKeywords}}">
    <meta name="robots" content="index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1">

    <meta property="og:title" content="{{ config('app.name', 'Know Academy') }} - @yield('title')" />
    <meta property="og:type" content="article" />
    <meta property="article:publisher" content="{{$siteFacebook}}">
    <meta name="author" content="{{ config('app.name', 'Know Academya') }}">
    <meta property="og:url" content="{{request()->url('/')}}" />
    <link rel="canonical" href="{{\Illuminate\Support\Facades\Request::url()}}"/>
    <meta property="og:site_name" content="{{ config('app.name', 'Know Academy') }}" />
    <meta property="og:image:alt" content="{{ config('app.name', 'Know Academy') }} - @yield('title')" />
    <meta property="og:locale" content="en_US">


    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#111c42">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#111c42">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#111c42">



    @stack('seo')


    <title>{{ config('app.name', 'Know Academy') }} - @yield('title')</title>

    <link href="{{mix('css/know.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw==" crossorigin="anonymous" referrerpolicy="no-referrer" />

</head>

<body>

@yield('content')
<script src="{{mix('js/know.js')}}" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(function() {
        $('.news-border').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });
    });

</script>

@stack('scripts')
</body>
</html>



