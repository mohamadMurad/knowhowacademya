@extends('layouts.front')
@section('title', 'Meet Our Instructors')
@section('content')


    @include('front.shared.nav',['class'=>''])


    <section class="instructors-section">
        <div class="container">
            <div class="section-title d-flex justify-content-between align-items-center mb-3">
                <h1 class="text-center m-0">Meet Our Instructors</h1>
                <a class="btn btn-secondary"
                   href="https://portal.knowhowacademya.uk/trainer/register"
                target="_blank">Join our Instructors</a>
            </div>
            <div class="section-content">
                <div class="instructors">
                    <div class="row gy-5">
                        @foreach($instructors as $ind)
                            <div class="col-12 col-md-6 col-lg-4">
                                <a href="{{route('instructors.show',$ind->id)}}">
                                <div class="instructors-card" >
                                    @if($ind->ProfileImage)
                                        <img loading="lazy"
                                            src="{{$ind->ProfileImage}}"

                                            class="img-fluid"
                                            alt="{{$ind->name}}"
                                        />
                                    @else
                                        <img loading="lazy"
                                            src="{{asset('img/User-avatar.png')}}"

                                            class="img-fluid"
                                            alt="img"
                                        />
                                    @endif

                                    <h3>{{$ind->name}}</h3>
                                    <h5>{{$ind->major}}</h5>
{{--                                    <p>{!! $ind->bio!!}</p>--}}
                                </div>
                                </a>
                            </div>
                        @endforeach


                    </div>
                </div>
            </div>
        </div>



    </section>
    @include('front.shared.footer')
@endsection


@push('seo')

    <meta name="description" content="Meet Our Instructors - {{config('app.name', 'Know Academya')}}"/>
    <meta property="og:description" content="Meet Our Instructors - {{config('app.name', 'Know Academya')}}" />
    @if(\Illuminate\Support\Facades\File::exists(public_path('header_image/'.$header_og_img)))

        <meta property="og:image" content="{{asset('header_image/'.$header_og_img)}}"/>
    @else
        <meta property="og:image" content="{{asset('img/header-img.webp')}}"/>
    @endif

@endpush
