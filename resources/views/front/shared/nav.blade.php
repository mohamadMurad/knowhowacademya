<!-- Navbar -->
<nav class="navbar fixed-top navbar-expand-lg know-navbar {{$class}}">
    <div class="container">
        <a class="navbar-brand d-flex align-items-center gap-2" href="{{route('home')}}">
            <img
                src="{{$class ? asset('./img/footer-logo.svg') : asset('./img/logo.svg')}}"
                alt="logo"
                class="d-inline-block align-text-top"
            />
            <div class="d-flex flex-column nav-title-text">
                <span>know how academy </span>
                <span>British Academy</span>
            </div>
        </a>
        <button
            class="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
        >
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">


                <li class="nav-item">
                    <a class="nav-link  {{Route::is('home') ? 'active' : ''}}" href="{{route('home')}}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  {{Route::is('front.news') ? 'active' : ''}}"
                       href="{{route('front.news')}}">News</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  {{Route::is('branches') ? 'active' : ''}}"
                       href="{{route('branches')}}">Branches</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  {{Route::is('centers') ? 'active' : ''}}"
                       href="{{route('centers')}}">Accredited centers</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  {{Route::is('instructors') ? 'active' : ''}}" href="{{route('instructors')}}">Instructors</a>
                </li>

{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link " href="{{route('home')}}#checkCertificate">check certificate</a>--}}
{{--                </li>--}}
                <li class="nav-item">
                    <a class="nav-link {{Route::is('contact.us') ? 'active' : ''}}" aria-current="page"
                       href="{{route('contact.us')}}"
                    >Contact us</a
                    >
                </li>
                <li class="nav-item">
                    <a class="nav-link {{Route::is('about.us') ? 'active' : ''}}" aria-current="page"
                       href="{{route('about.us')}}"
                    >About us</a
                    >
                </li>
            </ul>
        </div>
    </div>
</nav>
