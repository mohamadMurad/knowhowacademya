
<div class="floating-chat"  data-tooltip="Contact Us">
    <svg
        xmlns="http://www.w3.org/2000/svg"
        width="99"
        height="99"
        viewBox="0 0 99 99"
        fill="none"
    >
        <g filter="url(#filter0_d_1049_1473)">
            <circle cx="49.5" cy="49.5" r="44.5" fill="#CCAA44"/>
        </g>
        <path
            d="M49.5 77.7188C65.0848 77.7188 77.7188 65.0848 77.7188 49.5C77.7188 33.9152 65.0848 21.2812 49.5 21.2812C33.9152 21.2812 21.2812 33.9152 21.2812 49.5C21.2812 65.0848 33.9152 77.7188 49.5 77.7188Z"
            stroke="white"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
        />
        <path
            d="M49.5052 54.5087C49.5052 54.5087 51.2666 53.128 52.3454 53.6556C55.9627 55.4236 57.5429 60.7615 65.0898 56.677C59.9396 66.0482 51.7391 62.0543 49.5052 58.7744C47.2726 62.0556 39.0708 66.0469 33.9219 56.677C41.4674 60.7615 43.049 55.4236 46.6663 53.6543C47.7438 53.128 49.5052 54.5087 49.5052 54.5087Z"
            stroke="white"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
        />
        <path
            d="M67.1177 53.8879V47.4028"
            stroke="white"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
        />
        <path
            d="M58.4592 48.3491C61.1717 48.3491 63.3706 46.1502 63.3706 43.4377C63.3706 40.7253 61.1717 38.5264 58.4592 38.5264C55.7468 38.5264 53.5479 40.7253 53.5479 43.4377C53.5479 46.1502 55.7468 48.3491 58.4592 48.3491Z"
            stroke="white"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
        />
        <path
            d="M67.1177 44.4219C67.6613 44.4219 68.1021 43.9812 68.1021 43.4375C68.1021 42.8938 67.6613 42.4531 67.1177 42.4531C66.574 42.4531 66.1333 42.8938 66.1333 43.4375C66.1333 43.9812 66.574 44.4219 67.1177 44.4219Z"
            fill="#CCAA44"
        />
        <path
            d="M31.8838 53.8879V47.4028"
            stroke="white"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
        />
        <path
            d="M40.5408 48.3491C43.2532 48.3491 45.4521 46.1502 45.4521 43.4377C45.4521 40.7253 43.2532 38.5264 40.5408 38.5264C37.8283 38.5264 35.6294 40.7253 35.6294 43.4377C35.6294 46.1502 37.8283 48.3491 40.5408 48.3491Z"
            stroke="white"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
        />
        <path
            d="M31.8838 44.4219C32.4274 44.4219 32.8682 43.9812 32.8682 43.4375C32.8682 42.8938 32.4274 42.4531 31.8838 42.4531C31.3401 42.4531 30.8994 42.8938 30.8994 43.4375C30.8994 43.9812 31.3401 44.4219 31.8838 44.4219Z"
            fill="#CCAA44"
        />
        <path
            d="M24.7594 63.0977L21.2812 77.7189L35.9025 74.2408"
            stroke="white"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
        />
        <defs>
            <filter
                id="filter0_d_1049_1473"
                x="0"
                y="0"
                width="99"
                height="99"
                filterUnits="userSpaceOnUse"
                color-interpolation-filters="sRGB"
            >
                <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                <feColorMatrix
                    in="SourceAlpha"
                    type="matrix"
                    values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                    result="hardAlpha"
                />
                <feOffset/>
                <feGaussianBlur stdDeviation="2.5"/>
                <feComposite in2="hardAlpha" operator="out"/>
                <feColorMatrix
                    type="matrix"
                    values="0 0 0 0 0.466667 0 0 0 0 0.466667 0 0 0 0 0.466667 0 0 0 0.25 0"
                />
                <feBlend
                    mode="normal"
                    in2="BackgroundImageFix"
                    result="effect1_dropShadow_1049_1473"
                />
                <feBlend
                    mode="normal"
                    in="SourceGraphic"
                    in2="effect1_dropShadow_1049_1473"
                    result="shape"
                />
            </filter>
        </defs>
    </svg>

</div>
<div class="chat">
    <div class="chat-header text-center">
        <span>Assessment</span>
    </div>
    <div class="chat-body">
        <ul class="messages">

        </ul>
    </div>
    <div class="chat-footer">
        <ul class="questions">
            @foreach($chats as $chat)
                <li class="" data-id="1" data-answer="{{$chat->answer}}">{{$chat->question}}</li>

            @endforeach
        </ul>
{{--        <div class="input-group">--}}
{{--            <input--}}
{{--                type="text"--}}
{{--                class="form-control"--}}
{{--                placeholder="Type in your question"--}}
{{--                aria-label="Type in your question"--}}
{{--                aria-describedby="Type in your question"--}}
{{--            />--}}
{{--            <button class="send-message" type="button" id="button-addon1">--}}
{{--                <svg--}}
{{--                    xmlns="http://www.w3.org/2000/svg"--}}
{{--                    width="22"--}}
{{--                    height="19"--}}
{{--                    viewBox="0 0 22 19"--}}
{{--                    fill="none"--}}
{{--                >--}}
{{--                    <path--}}
{{--                        d="M8.13264 8.61546L1.09114 2.85493C0.601588 2.45437 0.824645 1.68024 1.41908 1.57534L1.51635 1.56389L21.0374 0.575538C21.1632 0.569106 21.2886 0.595434 21.4012 0.651949C21.5139 0.708464 21.6099 0.793233 21.68 0.897972C21.7501 1.00271 21.7918 1.12384 21.801 1.24951C21.8103 1.37519 21.7868 1.50112 21.7328 1.615L21.6785 1.71038L10.7566 17.9202C10.4026 18.4448 9.61037 18.2944 9.45084 17.7119L9.43083 17.6172L8.13264 8.61546L1.09114 2.85493L8.13264 8.61546ZM3.47623 2.92423L8.88889 7.35159L14.4929 4.18569C14.6462 4.09911 14.8256 4.07096 14.998 4.10645C15.1705 4.14193 15.3242 4.23865 15.4308 4.37871L15.4855 4.46162C15.5721 4.61503 15.6002 4.79463 15.5645 4.96716C15.5289 5.13969 15.4319 5.29345 15.2916 5.39998L15.2095 5.45415L9.60379 8.621L10.6031 15.5416L19.657 2.10499L3.47538 2.9247L3.47623 2.92423Z"--}}
{{--                        fill="#C1C1C1"--}}
{{--                    />--}}
{{--                </svg>--}}
{{--            </button>--}}
{{--        </div>--}}
    </div>
</div>

<audio preload="auto" src="{{asset('chat.ogg')}}" id="notificationAudio"></audio>
