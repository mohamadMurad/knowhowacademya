@extends('layouts.front')

@section('title', 'Our Branches')

@section('content')


    @include('front.shared.nav',['class'=>''])

    <section class="intro-single">
        <div class="container">
            <div class="section-title text-center text-uppercase">
                <h2>our branches</h2>
            </div>
            <div class="section-content">
                <div class="row gy-5 departments">
                    @foreach($branches as $city => $branch)
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="branch">
                                <div class="branch-city">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor"
                                         class="bi bi-geo-alt" viewBox="0 0 16 16">
                                        <path
                                            d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z"/>
                                        <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                                    </svg>
                                    <h4>{{$city}}</h4>
                                </div>
                                <div class="branch-location">
                                    <ul class="branch-list">
                                        @foreach($branch as $b)
                                            <li>
                                                {{$b->name}}
                                                <div class="">
                                                    @if($b->agent == null)
                                                        <p class="m-0">Soon</p>
                                                    @else
                                                        <p class="m-0">Address : {{$b->address}}</p>
                                                        <p class="m-0">Agent : {{$b->agent}}</p>
                                                        <p class="m-0">Tel : <a href="tel:{{$b->phone}}">{{$b->phone}}</a></p>
                                                    @endif

                                                </div>
                                            </li>
                                        @endforeach

                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach


                </div>
            </div>
        </div>
    </section>
    @include('front.shared.footer')
@endsection


@push('seo')
    <meta name="description" content="{{config('app.name', 'Know Academy') }} Branch Locator">
    <meta property="og:description" content="{{config('app.name', 'Know Academy') }} Branch Locator"/>
    @if(\Illuminate\Support\Facades\File::exists(public_path('header_image/'.$header_og_img)))

        <meta property="og:image" content="{{asset('header_image/'.$header_og_img)}}"/>
    @else
        <meta property="og:image" content="{{asset('img/header-img.webp')}}"/>
    @endif

@endpush
