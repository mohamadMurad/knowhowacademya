@extends('layouts.front')

@section('title', 'Contact Us')

@section('content')


    @include('front.shared.nav',['class'=>''])
    @include('front.shared.contact')
    @include('front.shared.footer')
@endsection


@push('seo')
    <meta name="description" content="{{trim(preg_replace('/\s\s+/', ' ',$contactUsDesc))}}">
    <meta property="og:description"
          content="{{trim(preg_replace('/\s\s+/', ' ',$contactUsDesc))}}"/>
    @if(\Illuminate\Support\Facades\File::exists(public_path('header_image/'.$header_og_img)))

        <meta property="og:image" content="{{asset('header_image/'.$header_og_img)}}"/>
    @else
        <meta property="og:image" content="{{asset('img/header-img.webp')}}"/>
    @endif

@endpush
