@extends('layouts.front')

@section('title', 'Contact Us')

@section('content')


    @include('front.shared.nav',['class'=>''])
    <section class="intro-single">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="title-single-box">
                        <h1 class="title-single text-center">About US</h1>
                        <p class="contact-desc"> {!! $aboutUs !!}</p>
                    </div>
                </div>

            </div>
        </div>
    </section>

    @include('front.shared.footer')
@endsection


@push('seo')

    <meta name="description" content="{{trim(preg_replace('/\s\s+/', ' ',strip_tags($aboutUs)))}}">
    <meta property="og:description"
          content="{{trim(preg_replace('/\s\s+/', ' ',strip_tags($aboutUs)))}}"/>
    @if(\Illuminate\Support\Facades\File::exists(public_path('header_image/'.$header_og_img)))

        <meta property="og:image" content="{{asset('header_image/'.$header_og_img)}}"/>
    @else
        <meta property="og:image" content="{{asset('img/header-img.webp')}}"/>
    @endif

@endpush
