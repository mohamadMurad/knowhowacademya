<!-- top courses -->
<section
    id="topCourses"
    class="top-courses"
    data-aos="fade-up"
    data-aos-duration="1500"
>
    <div class="container">
        <div class="section-title text-center">
            <h2>Accredited centers</h2>
        </div>
        <div class="section-content">
            <div class="row position-relative">
                <div class="swiper news p-5">
                    <div class="swiper-wrapper">
                        @foreach($centers as $center)
                            <div class="col-lg-4 swiper-slide">
                                <article class="overflow-hidden rounded-lg shadow transition hover:shadow-lg news-item">
                                    <img src="{{$center->getFirstMediaUrl('accreditCenter')}}" alt="{{$center->name}}"
                                         class="h-56 w-full object-cover"
                                         style="    object-fit: cover;    height: 14rem;    width: 100%;">

                                    <div class="bg-white p-4 sm:p-6">
                                        <h3 class="mt-0.5 text-bold text-lg text-gray-900 news-title" style="    font-size: 1.125rem;
    line-height: 1.75rem;
}">
                                            {{$center->name}}
                                        </h3>

                                        <p class="mt-2 text-sm leading-relaxed text-gray-500 line-clamp-3"
                                           style="-webkit-line-clamp: 3;">
                                            {!! $center->address!!}
                                        </p>
                                    </div>
                                </article>

                            </div>
                        @endforeach
                    </div>

                </div>

                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>

        </div>
    </div>
</section>
