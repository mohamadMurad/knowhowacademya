@extends('layouts.front')
@section('title', 'Home')

@section('content')

    @include('front.shared.nav',['class'=>null])

    <!-- Header -->
    <div class="header-root">
        <div class="img-overlay"></div>
        <header class="know-header">
            <div class="container">
                <div class="row header-row">
                    <div class="col-12 col-md-12 col-lg-6">
                        <div class="header-content">
                            <h1>know how <span>academy</span></h1>
                            <p>{!!  $siteDescription!!}</p>
                            <a class="btn btn-secondary p-2" style="font-size: 24px;"
                               href="https://portal.knowhowacademya.uk/student/register"
                               target="_blank">Scholar register</a>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-6">
                        <div class="header-img">
                            <div class="rectangle">
                                @if(\Illuminate\Support\Facades\File::exists(public_path('header_image/'.$header_img)))
                                    <img loading="lazy"
                                         src="{{asset('header_image/'.$header_img)}}"
                                         class="img-fluid"
                                         alt="header-img"
                                    />
                                @else
                                    <img loading="lazy"
                                         src="{{asset('./img/header-img.webp')}}"
                                         class="img-fluid"
                                         alt="header-img"
                                    />
                                @endif


                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-lg-9">
                        <nav class="d-flex header-nav">
                            <div class="container-fluid">
                                {{--                                <div class="d-flex align-items-center" id="headerNavSearch">--}}
                                {{--                                    <form class="d-flex search-form" action="{{route('search')}}" method="get"--}}
                                {{--                                          id="search_form_1">--}}
                                {{--                                        <input--}}
                                {{--                                            id="header-search-input"--}}
                                {{--                                            class="form-control me-2"--}}
                                {{--                                            type="search"--}}
                                {{--                                            placeholder="Search any course |"--}}
                                {{--                                            aria-label="Search any course |"--}}
                                {{--                                            name="name"--}}
                                {{--                                        />--}}
                                {{--                                        <button--}}
                                {{--                                            class="btn btn-secondary header-search-btn"--}}
                                {{--                                            type="submit"--}}
                                {{--                                        >--}}
                                {{--                                            Search--}}
                                {{--                                        </button>--}}
                                {{--                                    </form>--}}
                                {{--                                    <ul class="d-flex flex-row">--}}
                                {{--                                        <li class="nav-item dropdown">--}}
                                {{--                                            <select--}}
                                {{--                                                form="search_form_1"--}}
                                {{--                                                name="location"--}}
                                {{--                                                required--}}
                                {{--                                                class="form-select select1"--}}
                                {{--                                                aria-label="Default select example"--}}
                                {{--                                            >--}}
                                {{--                                                <option selected value="null">Location</option>--}}
                                {{--                                                @foreach($locations as $index => $loc)--}}
                                {{--                                                    <option class="text-capitalize"--}}
                                {{--                                                            value="{{$loc->location}}">{{$loc->location}}</option>--}}
                                {{--                                                @endforeach--}}
                                {{--                                            </select>--}}
                                {{--                                        </li>--}}
                                {{--                                        <li class="nav-item dropdown">--}}
                                {{--                                            <select--}}
                                {{--                                                form="search_form_1"--}}
                                {{--                                                required--}}
                                {{--                                                name="department"--}}
                                {{--                                                class="form-select select1"--}}
                                {{--                                                aria-label="Default select example"--}}
                                {{--                                            >--}}
                                {{--                                                <option selected value="null">Department</option>--}}
                                {{--                                                @foreach($departments as $index => $dep)--}}
                                {{--                                                    <option value="{{$dep->id}}">{{$dep->name}}</option>--}}
                                {{--                                                @endforeach--}}
                                {{--                                            </select>--}}
                                {{--                                        </li>--}}
                                {{--                                    </ul>--}}
                                {{--                                </div>--}}
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
    </div>

    @if(count($upcomingCourses) > 0)
        @include('front.upcoming',['upcomingCourses'=>$upcomingCourses])
    @endif

    @if(count($newses) > 0)
        @include('front.latestNews',['newses'=>$newses])
    @endif



    @if(count($courses) > 0)
        @include('front.topCourses',['courses'=>$courses])
    @endif


    <!-- search -->
    <section class="search" data-aos="fade-up" data-aos-duration="1500">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <p class="color-white">
                        Explore our training
                        <span>courses schedules with</span>
                        the search filters
                    </p>
                </div>
                <div class="col-md-12">
                    <form class="" action="{{route('search')}}" method="get" id="search_form_2">
                        <div class="d-flex align-items-stretch gap-4 from-search-div">
                            <div class="d-flex flex-column gap-2 inputs">
                                <div class="row gx-2 gy-2">
                                    <div class="col-12 col-md-6 col-lg-6">
                                        <select

                                            required
                                            name="department"
                                            class="form-select select1"
                                            aria-label="Default select example"
                                        >
                                            <option selected value="null">Choose Department</option>
                                            @foreach($departments as $index => $dep)
                                                <option value="{{$dep->id}}">{{$dep->name}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-6">
                                        <select
                                            name="location"
                                            required
                                            class="form-select"
                                            aria-label="Default select example"
                                        >
                                            <option selected value="null">Location</option>
                                            @foreach($locations as $index => $loc)
                                                <option class="text-capitalize"
                                                        value="{{$loc->location}}">{{$loc->location}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row gx-2 gy-2">
                                    <div class="col-12 col-md-6 col-lg-6">
                                        <select required
                                                name="month"
                                                class="form-select"
                                                aria-label="Default select example"
                                        >
                                            <option selected value="null">Choose Month</option>
                                            <option value='1'>January</option>
                                            <option value='2'>February</option>
                                            <option value='3'>March</option>
                                            <option value='4'>April</option>
                                            <option value='5'>May</option>
                                            <option value='6'>June</option>
                                            <option value='7'>July</option>
                                            <option value='8'>August</option>
                                            <option value='9'>September</option>
                                            <option value='10'>October</option>
                                            <option value='11'>November</option>
                                            <option value='12'>December</option>

                                        </select>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-6">
                                        <select
                                            name="all"
                                            class="form-select"
                                            aria-label="Default select example"
                                        >
                                            <option selected value="null">All</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="button ms-2 h-100">
                                <button
                                    class="btn btn-secondary no-hover"
                                    type="submit"
                                    id="button-addon2"
                                >
                                    SEARCH
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- department -->
    <section class="department" data-aos="fade-up" data-aos-duration="1500">
        <div class="container department-container">
            <div class="section-title text-center">
                <h2>Department section</h2>
            </div>
            <div class="section-content">
                <div class="row gy-5 departments">
                    @foreach($departments as $index => $dep)
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="department-card h-100">
                                <div class="department-num mb-3">
                                    @if(\Illuminate\Support\Facades\File::exists(public_path('department_image/'.$dep->image1)))
                                        <img id="img1" loading="lazy"
                                             class="img-fluid program-img rounded-1"
                                             src="{{asset('department_image/'.$dep->image1)}}"
                                             alt=""
                                        />
                                    @else
                                        <img id="img1" loading="lazy"
                                             class="img-fluid program-img"
                                             src="{{asset('img/footer-logo.svg')}}"
                                             alt="logo"
                                        />
                                    @endif
                                    {{--                                    <span>{{sprintf("%02d", $index+1)}}.</span>--}}
                                </div>
                                <div class="department-name">
                                    <h2> {{$dep->name}}</h2>
                                </div>
                                <div class="department-desc">
                                    <p>
                                        {!! ((\Illuminate\Support\Str::limit(strip_tags($dep->desc), 150, $end='...'))) !!}
                                    </p>
                                </div>
                                <div class="department-btn">
                                    <a class="btn btn-secondary"
                                       href="{{route('department.show',['department'=>$dep->slug])}}"
                                    >Read more</a
                                    >
                                </div>
                            </div>
                        </div>
                    @endforeach


                </div>
            </div>
        </div>
    </section>
    <!-- certificate -->
    <section
        id="checkCertificate"
        class="certificate"
        data-aos="fade-up"
        data-aos-duration="1500"
    >
        <div class="container">
            <div class="bg-logo">
                <img src="./img/bg-logo.svg" alt="bg-logo" loading="lazy"/>
            </div>
            <div class="row gy-2 gx-5 align-items-center">
                <div class="col-md-4">
                    <div class="w-100">
                        <h2 class="color-secondary">
                            Check your Certificate information
                        </h2>
                        <p class="color-white">
                            Your referance code can be found on the lower right section of
                            the certificate
                        </p>
                    </div>
                </div>
                <div class="col-md-8">
                    <form class="" id="" action="{{route('check_certificate')}}" method="post">
                        <div class="input-group mb-3 certificate-input-group">
                            <input
                                type="text"
                                required
                                name="cert_id"
                                class="form-control-lg input-color flex-grow-1"
                                placeholder="Type the certificate ID"
                                aria-label="Type the certificate ID"
                                aria-describedby="button-addon2"
                            />
                            <button
                                {{--                                data-bs-toggle="modal"--}}
                                {{--                                data-bs-target="#certificateModal"--}}
                                aria-label="search"
                                class="btn btn-secondary no-hover"
                                type="submit"
                                id="search-button"
                            >
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="22"
                                    height="22"
                                    fill="currentColor"
                                    class="bi bi-search"
                                    viewBox="0 0 16 16"
                                >
                                    <path
                                        d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"
                                    />
                                </svg>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    @if(count($centers) > 0)
        @include('front.latestCenters',['centers'=>$centers])
    @endif

    @include('front.shared.footer')

@endsection

@push('scripts')

    <script>
        $('.news-item').matchHeight();
        $("#certificate_form").submit(function (e) {

            e.preventDefault(); // avoid to execute the actual submit of the form.

            let form = $(this);
            let actionUrl = form.attr('action');
            console.log(form.serialize());
            $.ajax({
                type: "POST",
                url: actionUrl,
                data: form.serialize(), // serializes the form's elements.
                success: function (data) {

                    let status = data.data.status;
                    let msg = data.data.msg;
                    let info = data.data.info;
                    if (status) {
                        $('#modal-icon').attr('src', './img/success.gif');
                        $('#certificateModalText').html(msg);
                        $('#certificateModalInfo').html(info);
                        $('.know-modal').modal('show')
                        return;
                    }

                    $('#modal-icon').attr('src', './img/faild.gif');
                    $('#certificateModalText').html(msg);
                    $('#certificateModalInfo').html('');
                    $('.know-modal').modal('show')
                    return;


                },
                fail: function (s) {
                    // alert(s); // show response from the php script.
                }
            });

        });


    </script>
@endpush

@push('seo')
    <link rel="preload" as="image" href="{{asset('img/header-bg.webp')}}"/>
    @if(\Illuminate\Support\Facades\File::exists(public_path('header_image/'.$header_img)))

        <link rel="preload" as="image" href="{{asset('header_image/'.$header_img)}}"/>
    @else
        <link rel="preload" as="image" href="{{asset('./img/header-img.webp')}}"/>

    @endif

    <meta name="description" content="{{trim(preg_replace('/\s\s+/', ' ',strip_tags($siteDescription)))}}">
    <meta property="og:description" content="{{strip_tags($siteDescription)}}"/>
    @if(\Illuminate\Support\Facades\File::exists(public_path('header_image/'.$header_og_img)))

        <meta property="og:image" content="{{asset('header_image/'.$header_og_img)}}"/>
    @else
        <meta property="og:image" content="{{asset('img/header-img.webp')}}"/>
    @endif

@endpush
