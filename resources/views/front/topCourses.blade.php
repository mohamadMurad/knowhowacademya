<!-- top courses -->
<section
    id="topCourses"
    class="top-courses"
    data-aos="fade-up"
    data-aos-duration="1500"
>
    <div class="department-container">
        <div class="section-title text-center">
            <h2>OUR TOP COURSES</h2>
        </div>
        <div class="section-content">
            <div class="courses-slider">
                <div class="swiper top-courses-swiper">
                    <div class="swiper-wrapper align-items-center">
                        @foreach($courses as $course)
                            <div class="swiper-slide">
                                <div class="card course-slide-item">
                                    <a href="{{route('course.show',['course'=>$course->slug])}}">
                                        <div class="course-slide-bg">
                                            @if(\Illuminate\Support\Facades\File::exists(public_path('courses_cover/'.$course->cover)))
                                                <img loading="lazy"
                                                    src="{{asset('courses_cover/'.$course->cover)}}"

                                                    class="img-fluid"
                                                    alt="img"
                                                />
                                            @else
                                                <img loading="lazy"
                                                    src="./img/image.webp"

                                                    class="img-fluid"
                                                    alt="img"
                                                />
                                            @endif
                                        </div>
                                        <div class="course-slide-content">
                                            <span class="course-slide-price">{{$course->price}}</span>

                                            <h2 class="course-slide-name"> {{\Illuminate\Support\Str::limit($course->name, 50, $end='...')}}</h2>
                                            <div
                                                class="d-flex align-items-center gap-2 course-slide-location"
                                            >
                        <span class="icon">
                          <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="17"
                              height="24"
                              viewBox="0 0 17 24"
                              fill="none"
                          >
                            <path
                                d="M8.04131 0.408205C6.99436 0.407547 5.95755 0.613275 4.99017 1.01362C4.02278 1.41397 3.14381 2.00108 2.4035 2.74139C1.66319 3.4817 1.07608 4.36067 0.675731 5.32806C0.275384 6.29544 0.0696567 7.33225 0.0703141 8.3792C0.0703141 14.3582 8.04131 23.1792 8.04131 23.1792C8.04131 23.1792 16.0123 14.3542 16.0123 8.3792C16.013 7.33225 15.8072 6.29544 15.4069 5.32806C15.0065 4.36067 14.4194 3.4817 13.6791 2.74139C12.9388 2.00108 12.0598 1.41397 11.0925 1.01362C10.1251 0.613275 9.08827 0.407547 8.04131 0.408205ZM8.04131 11.2262C7.47823 11.2262 6.92779 11.0592 6.45961 10.7464C5.99142 10.4336 5.62651 9.98893 5.41103 9.46871C5.19555 8.94848 5.13917 8.37605 5.24902 7.82378C5.35887 7.27152 5.63002 6.76423 6.02818 6.36607C6.42634 5.96791 6.93363 5.69676 7.48589 5.58691C8.03816 5.47706 8.61059 5.53344 9.13081 5.74892C9.65104 5.9644 10.0957 6.32931 10.4085 6.7975C10.7213 7.26568 10.8883 7.81612 10.8883 8.3792C10.8881 9.13419 10.588 9.85819 10.0542 10.392C9.5203 10.9259 8.7963 11.2259 8.04131 11.2262Z"
                                fill="#012951"
                            />
                          </svg>
                        </span>
                                                <span class="text text-capitalize">{{$course->location}}</span>
                                            </div>
                                            <span class="course-slide-type">{{$course->department->name}}</span>
                                        </div>
                                    </a>

                                </div>
                            </div>
                        @endforeach


                    </div>
                    <div
                        class="course-button d-flex justify-content-end align-items-center gap-4 mt-3 pl-1 container"
                    >
                        <div class="top-courses-swiper-button-prev">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="51"
                                height="18"
                                viewBox="0 0 51 18"
                                fill="none"
                            >
                                <path
                                    opacity="0.72"
                                    d="M15.2608 12.27L49.6338 12.27C50.3878 12.27 51 11.8306 51 11.2888V6.71047C51 6.1687 50.3888 5.72924 49.6338 5.72924L15.2608 5.72924V1.96474C15.2608 0.220835 12.3182 -0.658091 10.5971 0.577753L0.800236 7.61265C0.563297 7.72657 0.36084 7.9191 0.218422 8.16594C0.0760002 8.41279 0 8.70288 0 8.99963C0 9.29638 0.0760002 9.58647 0.218422 9.83332C0.36084 10.0802 0.563297 10.2727 0.800236 10.3866L10.5971 17.4215C12.3182 18.6574 15.2608 17.7819 15.2608 16.0345V12.27Z"
                                    fill="#111C42"
                                />
                            </svg>
                        </div>
                        <div class="top-courses-swiper-button-next">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="51"
                                height="18"
                                viewBox="0 0 51 18"
                                fill="none"
                            >
                                <path
                                    opacity="0.72"
                                    d="M35.7392 5.72997L1.36623 5.72997C0.612241 5.72997 0 6.16944 0 6.71121L0 11.2895C0 11.8313 0.611236 12.2708 1.36623 12.2708L35.7392 12.2708V16.0353C35.7392 17.7792 38.6818 18.6581 40.4029 17.4222L50.1998 10.3874C50.4367 10.2734 50.6392 10.0809 50.7816 9.83406C50.924 9.58721 51 9.29712 51 9.00037C51 8.70362 50.924 8.41353 50.7816 8.16668C50.6392 7.91984 50.4367 7.72731 50.1998 7.61339L40.4029 0.578493C38.6818 -0.657352 35.7392 0.218086 35.7392 1.96548V5.72997Z"
                                    fill="#111C42"
                                />
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
