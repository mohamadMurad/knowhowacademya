<article class="overflow-hidden rounded-lg shadow transition hover:shadow-lg news-item">
    <img src="{{$news->cover}}" alt="{{$news->title}}" class="h-56 w-full object-fill" style="    object-fit: fill;    height: 14rem;    width: 100%;">

    <div class="bg-white p-4 sm:p-6">
        <time datetime="{{$news->created_at}}" class="block text-xs text-gray-500" style="font-size: .75rem;
    line-height: 1rem;">
           {{$news->created_at->diffForHumans()}}
        </time>

        <a href="{{route('front.news.show',$news->id)}}">
            <h3 class="mt-0.5 text-lg text-gray-900 news-title" style="    font-size: 1.125rem;
    line-height: 1.75rem;
}">
                {{$news->title}}
            </h3>
        </a>

        <p class="mt-2 text-sm leading-relaxed text-gray-500 line-clamp-3" style="-webkit-line-clamp: 3;">
            {{$news->short_title}}
        </p>
    </div>
</article>
