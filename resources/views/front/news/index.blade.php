@extends('layouts.front')

@section('title', 'News')

@section('content')

    @include('front.shared.nav',['class'=>''])
    <section class="intro-single">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="title-single-box">
                        <h1 class="title-single text-center">News</h1>

                    </div>
                </div>

            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row gy-4">
                @foreach($newses as $news)
                    <div class="col-lg-4">
                        @include('front.news.NewsItem',['news'=>$news])
                    </div>
{{--                    <div class="col-lg-4">--}}
{{--                        <div class="position-relative mb-3 border  news-border">--}}
{{--                            <img src="{{$news->cover}}" alt="{{$news->title}}" height="250" class=" w-100"--}}
{{--                                 style="object-fit:cover;">--}}
{{--                            <div class="bg-white p-4">--}}
{{--                                <div class="mb-2">--}}

{{--                                    <a class="text-body" href="{{route('front.news.show',$news->slug)}}">--}}
{{--                                        <small>{{$news->created_at->diffForHumans()}}</small>--}}

{{--                                    </a>--}}
{{--                                </div>--}}
{{--                                <a class="h4 d-block mb-3 text-uppercase font-weight-bold news-title"--}}
{{--                                   href="{{route('front.news.show',$news->slug)}}">{{$news->title}}</a>--}}
{{--                                <p class="m-0">--}}
{{--                                    {{$news->short_title}}--}}

{{--                                </p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                @endforeach

                @if(!$newses)
                    <p>There is no News</p>
                    @endif
            </div>

            {{$newses->links()}}
        </div>
    </section>

    @include('front.shared.footer')
@endsection


@push('seo')

    <meta name="description" content="Know How News">
    <meta property="og:description"
          content="Know How News"/>

    <meta property="og:image" content="{{asset('img/header-img.webp')}}"/>

@endpush
@push('scripts')
    <script>
        $('.news-item').matchHeight();
    </script>
@endpush
