@extends('layouts.front')

@section('title', 'News')

@section('content')

    @include('front.shared.nav',['class'=>''])
    <section class="intro-single">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="title-single-box">
                        <h1 class="title-single text-center">News</h1>

                    </div>
                </div>

            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    <!-- Trending Tittle -->
                    <div class="about-right mb-90">
                        <div class="news-cover img-fluid text-center">
                            <img class="img-fluid" src="{{$news->cover}}" alt="">
                        </div>
                        <div class=" heading-news mb-30 pt-30 text-center ">
                            <h3>{{$news->title}}</h3>
                        </div>
                        <div class="about-prea overflow-hidden">
                            {!!$news->body  !!}
                        </div>


                    </div>

                    @include('front.shared.share',['title'=>$news->title])
                </div>

            </div>

        </div>
    </section>

    @include('front.shared.footer')
@endsection

@push('seo')

    <meta name="description" content="{{$news->short_title}}"/>
    <meta property="og:description" content="{{$news->short_title}}"/>
    <meta property="og:image" content="{{$news->cover}}"/>

@endpush

