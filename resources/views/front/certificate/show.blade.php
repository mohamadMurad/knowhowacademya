@extends('layouts.front')

@section('title', 'Certificate')

@section('content')

    @include('front.shared.nav',['class'=>''])

    <section class="intro-single">
        <div class="container">

            <div class="section-content">
                <div class="row ">
                    @include('front.shared.share',['title'=>$certificate->cert_id])
                    {{--                    <embed--}}
                    {{--                        src="data:application/pdf;base64,{{$certificate->file}}"--}}
                    {{--                        type="application/pdf"--}}
                    {{--                        width="100%"--}}
                    {{--                        frameBorder="0"--}}
                    {{--                        scrolling="auto"--}}
                    {{--                        height="600px"/>--}}

                    {{--                    <iframe src="{{$certificate->file_path}}" title='SOME_TITLE'/>--}}

                    <canvas id="the-canvas"></canvas>


                </div>
            </div>
        </div>
    </section>
    @include('front.shared.footer')
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.6.347/pdf.min.js" integrity="sha512-Z8CqofpIcnJN80feS2uccz+pXWgZzeKxDsDNMD/dJ6997/LSRY+W4NmEt9acwR+Gt9OHN0kkI1CTianCwoqcjQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script type="text/javascript">
        var pdfData = atob('{{$certificate->file}}');
        var {pdfjsLib} = globalThis;
        //    he workerSrc property shall be specified.
        pdfjsLib.GlobalWorkerOptions.workerSrc = 'https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.6.347/pdf.worker.min.js';

        // Using DocumentInitParameters object to load binary data.
        var loadingTask = pdfjsLib.getDocument({data: pdfData});
        loadingTask.promise.then(function (pdf) {
            console.log('PDF loaded');

            // Fetch the first page
            var pageNumber = 1;
            pdf.getPage(pageNumber).then(function (page) {
                console.log('Page loaded');

                var scale = 1.5;
                var viewport = page.getViewport({scale: scale});

                // Prepare canvas using PDF page dimensions
                var canvas = document.getElementById('the-canvas');
                var context = canvas.getContext('2d');
                canvas.height = viewport.height;
                canvas.width = viewport.width;

                // Render PDF page into canvas context
                var renderContext = {
                    canvasContext: context,
                    viewport: viewport
                };
                var renderTask = page.render(renderContext);
                renderTask.promise.then(function () {
                    console.log('Page rendered');
                });
            });
        }, function (reason) {
            // PDF loading error
            console.error(reason);
        });
    </script>
@endpush

@push('seo')
    <meta name="description" content="{{ 'This certificate is verified by '.config('app.name') }} ">
    <meta property="og:description" content="{{'This certificate is verified by '.config('app.name') }} "/>
    @if(\Illuminate\Support\Facades\File::exists(public_path('header_image/'.$header_og_img)))

        <meta property="og:image" content="{{asset('header_image/'.$header_og_img)}}"/>
    @else
        <meta property="og:image" content="{{asset('img/header-img.webp')}}"/>
    @endif

@endpush
