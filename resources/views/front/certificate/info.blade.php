@extends('layouts.front')

@section('title', 'Certificate')

@section('content')

    @include('front.shared.nav',['class'=>''])

    <section class="intro-single">
        <div class="container">
            {{--            <div class="section-title text-center text-uppercase">--}}
            {{--                @if($certificate)--}}
            {{--                    <h2>Verified</h2>--}}
            {{--                @else--}}
            {{--                    <h2>Not Verified</h2>--}}
            {{--                @endif--}}
            {{--            </div>--}}
            <div class="section-content">
                <div class="row ">
                    <div class="card shadow-blur shadow-sm">
                        <div class="card-body p-5 text-center">
                            @if($certificate)
                                <h3 class="text-center"> This certificate is verified by <b>{{config('app.name')}}</b>
                                </h3>
                                <div class="msg-icon text-center">
                                    <img class="img-fluid" loading="lazy" id="modal-icon"
                                         src="{{asset('img/success.gif')}}"
                                         alt="success">
                                </div>
                                <p>{!! $certificate->info !!}</p>
                                @if($certificate->file)
                                    <a href="{{route('front.certificate.show',$certificate->uuid)}}"
                                       class="btn btn-secondary">Show
                                        Certificate</a>
                                @endif

                            @else
                                <h3 class="text-center"> This certificate is not verified by
                                    <b>{{config('app.name')}}</b></h3>
                                <div class="msg-icon text-center">
                                    <img class="img-fluid" loading="lazy" id="modal-icon"
                                         src="{{asset('img/faild.gif')}}"
                                         alt="faild">
                                </div>
                            @endif


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('front.shared.footer')
@endsection


@push('seo')
    <meta name="description" content="{{ 'This certificate is verified by '.config('app.name') }} ">
    <meta property="og:description" content="{{'This certificate is verified by '.config('app.name') }} "/>
    @if(\Illuminate\Support\Facades\File::exists(public_path('header_image/'.$header_og_img)))

        <meta property="og:image" content="{{asset('header_image/'.$header_og_img)}}"/>
    @else
        <meta property="og:image" content="{{asset('img/header-img.webp')}}"/>
    @endif

@endpush
