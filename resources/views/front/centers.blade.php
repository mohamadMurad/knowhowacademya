@extends('layouts.front')

@section('title', 'Our Branches')

@section('content')

    @include('front.shared.nav',['class'=>''])

    <section class="intro-single">
        <div class="container">
            <div class="section-title text-center text-uppercase">
                <h2>Our Accredited centers</h2>
            </div>
            <div class="section-content">
                <div class="row gy-5 departments">

                    @foreach($centers as $center)
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="branch">
                                <div class="branch-location">
                                    @if($center->hasMedia('accreditCenter'))
                                        <img src="{{$center->getFirstMediaUrl('accreditCenter')}}"
                                             alt="profile_image"
                                             class="w-100 mb-2  border-radius-lg shadow-sm img-fluid object-fit-contain" style="    object-fit: contain;height:150px;">
                                    @else
                                        <img src="{{asset('img/header-img.webp')}}"
                                             alt="profile_image"
                                             class="w-100 mb-2 border-radius-lg shadow-sm img-fluid object-fit-contain" style="    object-fit: contain;height:150px;">
                                    @endif
                                    <ul class="branch-list">


                                        <li>
                                            {{$center->name}}
                                            <div class="">

                                                <p class="m-0">Address : {!! $center->address !!}</p>

                                                <p class="m-0">Tel : <a
                                                        href="tel:{{$center->phone}}">{{$center->phone}}</a>
                                                </p>


                                            </div>
                                        </li>


                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach


                </div>
            </div>
        </div>
    </section>
    @include('front.shared.footer')
@endsection


@push('seo')
    <meta name="description" content="{{config('app.name', 'Know Academy') }} Accredited centers ">
    <meta property="og:description" content="{{config('app.name', 'Know Academy') }} Accredited centers "/>
    @if(\Illuminate\Support\Facades\File::exists(public_path('header_image/'.$header_og_img)))

        <meta property="og:image" content="{{asset('header_image/'.$header_og_img)}}"/>
    @else
        <meta property="og:image" content="{{asset('img/header-img.webp')}}"/>
    @endif

@endpush
