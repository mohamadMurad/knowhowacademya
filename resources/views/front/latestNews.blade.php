<!-- top courses -->
<section
    id="topCourses"
    class="top-courses"
    data-aos="fade-up"
    data-aos-duration="1500"
>
    <div class="container">
        <div class="section-title text-center">
            <h2>Latest News</h2>
        </div>
        <div class="section-content">
            <div class="row position-relative">
                <div class="swiper news p-5">
                    <div class="swiper-wrapper">
                        @foreach($newses as $news)
                            <div class="col-lg-4 swiper-slide">
                                @include('front.news.NewsItem', ['news' => $news])
{{--                                <div class="position-relative mb-3  border  news-border">--}}
{{--                                    <img src="{{$news->cover}}" alt="{{$news->title}}" height="250" class=" w-100"--}}
{{--                                         style="object-fit:cover;">--}}
{{--                                    <div class="bg-white p-4 ">--}}
{{--                                        <div class="mb-2">--}}

{{--                                            <a class="text-body" href="{{route('front.news.show',$news->slug)}}">--}}
{{--                                                <small>{{$news->created_at->diffForHumans()}}</small>--}}

{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                        <a class="h4 d-block mb-3 text-uppercase font-weight-bold news-title"--}}
{{--                                           href="{{route('front.news.show',$news->slug)}}">{{$news->title}}</a>--}}
{{--                                        <p class="m-0">--}}
{{--                                            {{$news->short_title}}--}}

{{--                                        </p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </div>
                        @endforeach
                    </div>

                </div>

                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>

        </div>
    </div>
</section>
