global.$ = global.jQuery = require("jquery");
window.$ = window.jQuery = require("jquery");
import "bootstrap";
import AOS from "aos";

import "select2";
import 'jquery-match-height/dist/jquery.matchHeight-min';

AOS.init({
    once: true,
});

// core version + navigation, pagination modules:
import Swiper, {Navigation, Pagination,Autoplay} from "swiper";
Swiper.use(Autoplay);

const swiper1 = new Swiper(".upcoming-swiper", {
    slidesPerView: "auto",
    centeredSlides: true,
    modules: [Navigation, Pagination],
    speed: 600,
    // spaceBetween: 30,
    loop: true,
    autoplay: {
        delay: 3000,

    },
    pagination: {
        el: ".upcoming-swiper-pagination",
        type: "fraction",
        formatFractionCurrent: function (number) {
            return "0" + number;
        },
        formatFractionTotal: function (number) {
            return "0" + number;
        },
    },
    navigation: {
        nextEl: ".upcoming-swiper-button-next",
        prevEl: ".upcoming-swiper-button-prev",
    },
});

const swiper2 = new Swiper(".top-courses-swiper", {
    slidesPerView: "auto",
    centeredSlides: true,
    modules: [Navigation, Pagination],
    speed: 600,
    loop: true,
    autoplay: {
        delay: 3000,

    },

    //loopedSlides: 3,
   // centeredSlidesBounds: true,


    //spaceBetween: 30,
    // pagination: {
    //   el: ".swiper-pagination",
    //   clickable: true,
    // },
    navigation: {
        nextEl: ".top-courses-swiper-button-next",
        prevEl: ".top-courses-swiper-button-prev",
    },
});

const swiper = new Swiper(".news", {
    slidesPerView: 1,
    spaceBetween: 30,


    loopFillGroupWithBlank: true,
    modules: [Navigation, Pagination],
    speed: 600,
    loop: true,
    autoplay: {
        delay: 3000,

    },
    // pagination: {
    //     el: ".swiper-pagination",
    //     clickable: true,
    // },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    breakpoints: {
        640: {
            slidesPerView: 1,
            spaceBetween: 20,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 40,
        },
        1024: {
            slidesPerView: 3,
            spaceBetween: 50,
        },
    }


});

$('.department-desc').matchHeight();
$('.instructors-card').matchHeight();
$('.branch').matchHeight();

let chat = $(".chat");

$(document).ready(function () {
     // code here
    initMsg();
});


$(".floating-chat").on("click", function () {
    scrollToEndChat();
    $(this).removeClass('active');
    // if ($(".chat.open")) {

    //   chat.removeClass("open");
    // }
    chat.addClass("open");
});
//  var parentElement = $(".header-nav");
$("select").select2({
    minimumResultsForSearch: -1,

    //  dropdownParent: parentElement
});

scrollToEndChat();
$(document).mouseup(function (e) {
    var container = $(".chat");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        container.removeClass("open");
    }
});

$(".questions li").on("click", function () {
    let $text = $(this).html();
    let $answer = $(this).data("answer");

    if ($text != "") {
        sendMessage($text);
        setTimeout(function () {
            addTypingMsg();
        }, 1000);
        setTimeout(function () {
            if (!$('.chat').hasClass('open')) {
                $('.floating-chat').attr('data-tooltip', $answer.slice(0, 30) + "...");
                $('.floating-chat').addClass('active');
            }
            $('#notificationAudio').trigger('play');
            editTypingMsg($answer.replace(/\n/g, "<br />"));


        }, 2500);
    }
});

$(".send-message").on("click", function () {
    let $text = $(this).siblings("input").val();
    //console.log($text);
    $(this).siblings("input").val("");
    if ($text != "") {
        sendMessage($text);
        setTimeout(function () {
            addTypingMsg();
        }, 1000);
        setTimeout(function () {
            if (!$('.chat').hasClass('open')) {
                $('.floating-chat').attr('data-tooltip', "no data");
                $('.floating-chat').addClass('active');
            }
            $('#notificationAudio').trigger('play');
            editTypingMsg("no data");
        }, 2500);
    }
});

function sendMessage($text) {
    $(".messages").append('<li class="self">' + $text + "</li>");
    $(".messages")
        .finish()
        .animate(
            {
                scrollTop: $(".messages").prop("scrollHeight"),
            },
            250
        );
}

function addTypingMsg() {
    $(".messages").append(
        '<li class="other typing"> <span></span>  <span></span>  <span></span></li>'
    );
    $(".messages")
        .finish()
        .animate(
            {
                scrollTop: $(".messages").prop("scrollHeight"),
            },
            250
        );
}

function initMsg() {
    let msg = "Hi 👋 How Can I Help You";
    $(".messages").append(
        '<li class="other">' + msg + '</li>'
    );

    $(".messages")
        .finish()
        .animate(
            {
                scrollTop: $(".messages").prop("scrollHeight"),
            },
            250
        );

    if (!$('.chat').hasClass('open')) {
        $('.floating-chat').attr('data-tooltip', msg);
        $('.floating-chat').addClass('active');
    }
    // $('#notificationAudio').trigger('play');
}

function editTypingMsg($text) {
    $(".messages li.other.typing").html($text);
    $(".messages")
        .finish()
        .animate(
            {
                scrollTop: $(".messages").prop("scrollHeight"),
            },
            250
        );
}

function receveMessage($id) {
    $text = $id;
    $(".messages").append('<li class="other">' + $text + "</li>");
    $(".messages")
        .finish()
        .animate(
            {
                scrollTop: $(".messages").prop("scrollHeight"),
            },
            250
        );
}

function scrollToEndChat() {
    $(".messages").scrollTop($(".messages")[0].scrollHeight);
}
